import { SumoAppPage } from './app.po';

describe('sumo-app App', () => {
  let page: SumoAppPage;

  beforeEach(() => {
    page = new SumoAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

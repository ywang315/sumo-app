import { Contact } from '../contacts/contact';
import { GSECCode } from '../gseccodes/gseccode';
import { MonthlyExpenses } from '../monthlyexpenses/monthlyexpenses';
export class Trader {
    constructor (
        public Oid?: string,
        public Created?: Date,
        public Group?: string,
        public BeginningMonthlyCharge?: number | string,
        public GSECCodes: GSECCode[] = [],
        public Contacts: Contact[] = [],
        public MonthlyExpenses: MonthlyExpenses[] = []
    ){ }

}
import { Trader } from './trader';
import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { Contact } from '../contacts/contact';
import { Address } from '../contacts/address';
import { Phone } from '../contacts/phone';
import { Email } from '../contacts/email';
import { GSECCode } from '../gseccodes/gseccode';
import { ContactDetailComponent } from '../contacts/contact-detail.component'
import { TraderService } from './trader.service';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { ActivatedRoute, Params, Router } from '@angular/router';


@Component({
    moduleId: module.id,
    selector: 'trader-form',
    templateUrl: 'trader-form.component.html',
    styleUrls: ['trader-form.component.css']
})
export class TraderFormComponent implements OnInit{
    message: string;
    errorMessage: string;

    @Input() trader: Trader;
    title: string;
    edit: boolean = false
    gsDisabledList: boolean[] = []
    gsToBeDeleted: string[] = []

    constructor(
        private traderService: TraderService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        public snackBar: MatSnackBar
    ){ }

    addContact(){
        let contact = new Contact();
        contact.Addresses.push(new Address())
        contact.Phones.push(new Phone())
        contact.Emails.push(new Email())
        this.trader.Contacts.push(contact)
    }

    deleteContact(index: number){
        this.trader.Contacts.splice(index, 1)
    }

    deleteGSEC(index: number){
        if(! this.gsDisabledList[index]){
            this.trader.GSECCodes.splice(index, 1)
        }
    }

    addGSECCode(){
        this.trader.GSECCodes.push(new GSECCode())
        this.gsDisabledList.push(false)
    }


    save(){
        this.traderService.create(this.trader)
        .subscribe(
            trader => {
                this.trader = trader
                this.message = "Created"
                console.log("created")
            },
            error => {
                this.message = "Not Created"
                this.errorMessage = <any>error
            }
        )
    }
    update(){
        console.log(JSON.stringify(this.trader))
        
        this.traderService.update(this.trader).subscribe(
            trader => {
                this.trader = trader
                //use snack bar to show message

            },
            error => {
                this.errorMessage = <any> error
                //use snack bar to show message
            }
        )
        
    }

    getTrader(Oid:string){
        this.traderService.getTrader(Oid).subscribe(
            trader => {
                this.trader = trader
                this.trader.GSECCodes.forEach(
                    gs => this.gsDisabledList.push(true)
                )
            },
            error => {
                this.errorMessage = <any> error
            }
        )
    }

    init_trader_creation(){
        this.trader = new Trader()
        this.title = 'NEW TRADER'
    }
    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
        duration: 2000,
        });
    }

    ngOnInit(){
        let Oid = this.route.snapshot.params['Oid'];
        if (Oid == undefined){
            this.init_trader_creation()
        } else{
            this.getTrader(Oid)
            this.edit = true

        }
    }
}
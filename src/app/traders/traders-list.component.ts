import { Trader } from './trader';
import { Component, Input, OnInit } from '@angular/core';
import { TraderService } from './trader.service';
import { GSECCode } from '../gseccodes/gseccode';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    templateUrl: 'traders-list.component.html',
    styleUrls: ['traders-list.component.css']
})
export class TradersListComponent implements OnInit {
    traders: Trader[];
    selectedTrader: Trader;
    traderOid: string;
    errorMessage: string;
    deleteDisabled: boolean = false;
    //months = ['JAN','FEB','MAR','API','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']

    constructor( 
        private traderService: TraderService,
        private router: Router
        ) {  }

    getTraders(){
        this.traderService.getTraders()
        .subscribe(
            traders => this.traders = traders,
            error => this.errorMessage = <any>error
        )
    }

    getTrader(){
        this.traderService.getTrader(this.traderOid).subscribe(
            trader => {
                this.selectedTrader = trader
                trader.GSECCodes.forEach(
                    gs =>{
                        if(gs.ChargeOids.length > 0 || gs.CreditOids.length > 0){
                            this.deleteDisabled = true;
                        }
                    }
                )
            },
            error => this.errorMessage = <any>error
        )
    }

    updateTrader(Oid: string){
        let path = `/traders/${Oid}`
        this.router.navigate([path])
    }







    ngOnInit(){
        this.getTraders();
    }
}
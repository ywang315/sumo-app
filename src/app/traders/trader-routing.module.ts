import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraderFormComponent } from './trader-form.component';
import { TradersListComponent } from './traders-list.component';
import { AuthGuard } from '../auth/auth.guard'


const tradersRoutes: Routes =[
    {path: 'traders', component: TradersListComponent, canActivate: [AuthGuard]},
    {path: 'traders/:Oid', component: TraderFormComponent, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [
        RouterModule.forChild(tradersRoutes)
    ],
    exports: [
        RouterModule 
    ]
})
export class TradersRoutingModule { }
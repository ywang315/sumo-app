import { Component, Input, OnInit } from '@angular/core';
import { Trader } from './trader';
import { TraderService } from './trader.service';
import { MonthlyExpensesDetailsComponent } from '../monthlyexpenses/monthlyexpenses-details.component';
import { ContactDisplayComponent } from '../contacts/contact-display.component';
import { WorkingYearService } from '../local-store/working-year.service';


@Component({
    moduleId: module.id,
    selector: 'trader-detail',
    templateUrl: 'trader-detail.component.html',
    styleUrls: ['trader-detail.component.css']
})

export class TraderDetailComponent {
    constructor(private wys: WorkingYearService){}
    @Input() trader: Trader;
    year: string = this.wys.getYear() +"";
}
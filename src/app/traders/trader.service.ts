import { Trader } from './trader';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';


@Injectable()
export class TraderService {
    private traderUrl = `${HOST}/traders`
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: AuthHttp){ };


    create(trader: Trader): Observable<Trader> {
        return this.http.post(this.traderUrl,JSON.stringify(trader), {headers: this.headers})
        .map(
            res => {
                let body = <Trader> res.json();
                return body || {};
            }
        )
        .catch(this.handleError)
    }

    update(trader: Trader): Observable<Trader> {
        let url = `${this.traderUrl}/${trader.Oid}`
        return this.http.put(url, JSON.stringify(trader), {headers: this.headers})
                        .map(
                            res => {
                                let body = <Trader> res.json();
                                return body || {};
                            }
                        ).catch(this.handleError)
    }

    getTraders(): Observable<Trader[]> {
        return this.http.get(this.traderUrl)
        .map(
            res => {
                let body = <Trader[]> res.json();
                return body ||[]
            }
        )
        .catch(this.handleError)
    }

    getTrader(Oid: string): Observable<Trader> {
        let url = `${this.traderUrl}/${Oid}`
        return this.http.get(url).map(
            res => {
                let body = <Trader> res.json();
                return body || {}
            }
        ).catch(this.handleError);
    }


  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
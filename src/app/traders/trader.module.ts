import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { TraderFormComponent } from './trader-form.component';
import { TraderService } from './trader.service';
import { TradersRoutingModule } from './trader-routing.module';
import { TraderDetailComponent } from './trader-detail.component';
import { TradersListComponent } from './traders-list.component';
import { GSECCodeFormComponent } from '../gseccodes/gseccode-form.component';
import { ContactsModule } from '../contacts/contacts.module';
import { MonthlyExpensesDetailsComponent } from '../monthlyexpenses/monthlyexpenses-details.component';
//import { MdCardModule } from '@angular/material';
//import { MaterialModule } from '@angular/material';
import { GSECCodeModule } from '../gseccodes/gseccode.module';
import { LocalStoreModule } from '../local-store/local-store.module';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';



@NgModule({
    imports: [
        MatSelectModule,
        MatTooltipModule,
        NoopAnimationsModule,
        MatButtonModule, 
        MatCheckboxModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatSnackBarModule,
        MatTabsModule,

        //MaterialModule,
        //MdCardModule,
        CommonModule,
        FormsModule,
        TradersRoutingModule,
        ContactsModule,
        GSECCodeModule,
        LocalStoreModule,
        MatToolbarModule
    ],
    declarations: [
        TraderFormComponent,
        TraderDetailComponent,
        TradersListComponent,
        MonthlyExpensesDetailsComponent
    ],
    providers: [ 
        TraderService 
        ],
    exports: [
        TraderFormComponent, 
        MonthlyExpensesDetailsComponent
    ]

})
export class TradersModule {}

export class Breakdown {
    constructor(
        public GSECCode?:string,
        public CounterAccount?:string,
        public Detail?:string,
        public Trailer?:string,
        public Notes?: string,
        public Amount?:number | string,
        public Oid?:string,
        public Created?:Date,
        public CreatedBy?:string
    ){}
    
}



import { Breakdown } from './breakdown';
import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export  class BreakdownService {
    private billUrl = `${HOST}/bills`
    private headers = new Headers({'Content-Type': 'application/json'})
    constructor(private http: AuthHttp){ };

    addBreakdowns(billOid:string, breakdowns: Breakdown[]):Observable<Breakdown[]>{
        let url = `${this.billUrl}/${billOid}/breakdowns`
        return this.http.post(url, JSON.stringify(breakdowns),{headers: this.headers})
        .map(res => {
            let body = Array<Breakdown> res.json();
            return body || [];
        })
        .catch(this.handleError)
    }
    
    addBreakdown(billOid:string, breakdown: Breakdown):Observable<Breakdown>{
        let url = `${this.billUrl}/${billOid}/breakdown`
        return this.http.post(url,JSON.stringify(breakdown), {headers: this.headers})
        .map(res => {
            let body = <Breakdown> res.json();
            return body || {};
        })
        .catch(this.handleError)
    }
    updateBreakdown(breakdown: Breakdown):Observable<Breakdown>{
        let url = `${this.billUrl}/breakdowns/${breakdown.Oid}`
        return this.http.put(url, JSON.stringify(breakdown), {headers: this.headers})
        .map(res => {
            let body = <Breakdown> res.json();
            return body || {};
        })
        .catch(this.handleError)
    }

    delete(Oid: string): Observable<Breakdown>{
        let url = `${this.billUrl}/breakdowns/${Oid}`
        return this.http.delete(url, {headers: this.headers})
        .map(res => {
            let body = <Breakdown> res.json();
            return body || {}
        })
        .catch(this.handleError)
    }








  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
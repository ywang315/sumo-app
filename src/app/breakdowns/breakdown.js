"use strict";
var Breakdown = (function () {
    function Breakdown(GSECCode, CounterAccount, Detail, Trailer, Amount, Oid, Created, CreatedBy) {
        this.GSECCode = GSECCode;
        this.CounterAccount = CounterAccount;
        this.Detail = Detail;
        this.Trailer = Trailer;
        this.Amount = Amount;
        this.Oid = Oid;
        this.Created = Created;
        this.CreatedBy = CreatedBy;
    }
    return Breakdown;
}());
exports.Breakdown = Breakdown;
//# sourceMappingURL=breakdown.js.map
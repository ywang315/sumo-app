import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Breakdown } from './breakdown';
import { BreakdownService } from './breakdown.service';
import { GSECCodeService } from '../gseccodes/gseccode.service';
import { GSECCode } from '../gseccodes/gseccode';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import 'rxjs/add/operator/switchMap';

import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { ValidatorFn } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';


// custom validator to test the entered gsec code is in the list
export function forbiddenCodeValidator(codes: string[]): ValidatorFn {
    return (control: AbstractControl) : {[key: string]: any} => {
        let isFound:boolean = false;
        codes.forEach(code => {
            if (code.toUpperCase() === control.value){
                isFound = true;
            }
        })
        if(isFound){
            return null;
        }
        return {"inValidCode" : {value: control.value, status: "INVALID"}};
    }
}

@Component({
    moduleId: module.id,
    selector: 'bill-breakdown',
    templateUrl: 'breakdown.component.html'
})
export class BreakdownComponent implements OnInit {

    @Input()
    breakdown: Breakdown;
    @Input()
    services: string[] = [];

    @Input()
    gseccodes: string[]=[];
    errorMessage: string;


    gseccodeCtrl: FormControl = new FormControl(); //autocomplete control
    filteredCodes: Observable<string[]>;
    
    breakdownForm: FormGroup;

    constructor(
        private gseccodeService: GSECCodeService,
        public snackBar: MatSnackBar,
        private fb: FormBuilder
    ) {}

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
        duration: 2000,
        });
    }

    @Output() sendToBill: EventEmitter<Breakdown> = new EventEmitter<Breakdown>();
    addToBill(){
        this.sendToBill.emit(this.breakdown)
        this.breakdown = new Breakdown()
        this.breakdownForm.reset()
    }


    
    getGSECCodes(){
        this.gseccodeService.getGSECCodelist().subscribe(
            gseccodes => {
                this.gseccodes = gseccodes;

                this.createBreakdownFormGroup()

                this.filteredCodes = this.gseccodeCtrl.valueChanges
                .pipe(
                    startWith(''),
                    map(code  => code ? this.filterGSECCode(code) : this.gseccodes.slice())
                )
                
                },
            error => this.errorMessage = <any>error
        )
    }

    //gsec code auto complete
    filterGSECCode(code: string) {
        return this.gseccodes.filter(gseccode =>
            gseccode.toUpperCase().indexOf(code.toUpperCase()) === 0)
    }

    createBreakdownFormGroup(){
        this.breakdownForm = this.fb.group({
            'gseccode': new FormControl("", [
                Validators.required,
                forbiddenCodeValidator(this.gseccodes)
            ]),
            'amount': new FormControl("", [Validators.required])
        })
    }
 
    
   

    

    ngOnInit(){
        if(this.gseccodes.length === 0){
            this.getGSECCodes()
        } else {
            this.createBreakdownFormGroup()
            this.filteredCodes = this.gseccodeCtrl.valueChanges
            .pipe(
                startWith(''),
                map(code  => code ? this.filterGSECCode(code) : this.gseccodes.slice())
            )   
        }
        
       
        if (this.breakdown == null){
            this.breakdown =  new Breakdown();
        }
        this.createBreakdownFormGroup()
    }
    get gseccode() {return this.breakdownForm.get('gseccode');}
    get amount() {return this.breakdownForm.get('amount');}
        
}
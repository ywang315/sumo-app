import { MatTableDataSource, MatSort } from '@angular/material';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Breakdown } from './breakdown';
import {Sort} from '@angular/material';


@Component({
    moduleId: module.id,
    selector: 'breakdown-table',
    styleUrls: ['breakdown-table.component.css'],
    templateUrl: 'breakdown-table.component.html'
})
export class BreakdownTableComponent {
    displayedColumns = ['CreatedBy','GSECCode', 'Detail', 'Amount', 'Notes', 'Trailer']
    @Input() breakdowns:Breakdown[]

    dataSource:MatTableDataSource<Breakdown>;
    rows:number;

    @ViewChild(MatSort) sort: MatSort;

    @Output() removeFromBill: EventEmitter<number> = new EventEmitter<number>();
    remove(index: number){
        this.removeFromBill.emit(index)
    }


    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    }
    ngOnInit(){
        if(this.breakdowns){
            this.dataSource = new MatTableDataSource(this.breakdowns);
            this.rows = this.breakdowns.length
        }
    }
    ngDoCheck(){
        if(this.breakdowns.length !== this.rows){
            this.dataSource = new MatTableDataSource(this.breakdowns);
            this.rows = this.breakdowns.length
            this.dataSource.sort = this.sort;
        }
        
    }




}


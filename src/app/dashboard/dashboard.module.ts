import { NgModule } from '@angular/core';
//import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { BillStatComponent } from './bill-stat/bill-stat.component';
import { DashboardService } from './dashboard.service';
import { BillAmountsComponent } from './bill-stat/bill-amounts.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { LocalStoreModule } from '../local-store/local-store.module';

@NgModule({
    imports: [
        LocalStoreModule,
        //MaterialModule,
        CommonModule,
        FormsModule,
        ChartsModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger' // set defaults here
          })
    ],
    declarations: [
       DashboardComponent,
       BillStatComponent,
       BillAmountsComponent
    ],
    providers: [ DashboardService ],
    exports: [
        
    ]
})
export class DashboardModule {}
import { Component, OnInit} from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { Color } from 'ng2-charts';
import { BillStat } from './bill-stat';
import { WorkingYearService } from '../../local-store/working-year.service';


@Component({
    moduleId: module.id,
    selector: 'bill-stat',
    templateUrl: 'bill-stat.component.html',
    styleUrls: ['bill-stat.component.css']
})
export class BillStatComponent implements OnInit {

  errorMessage:string;
  billStat: BillStat;
  //year: string | number = (new Date().getFullYear()) + "";
  year: string | number = this.wys.getYear() + "";

  constructor(
    private dbService: DashboardService,
    private wys: WorkingYearService
  ){
  }
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  public barChartColor:Array<Color> =[
    {
      backgroundColor: '#FF402C'
    },
    {
      backgroundColor:'#4BD762'
    },
    {
      backgroundColor: '#FFCA1F'
    },
    {
      backgroundColor: '#278ECF'
    }

  ]
 
  public barChartData:any[];
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
 
  public randomize():void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }


  getBarchartData(year){
    this.dbService.getBarchartData(year).subscribe(
      barchartData =>{
        this.barChartData = barchartData
      },
      error => this.errorMessage = <any>error
    )
  }

    ngOnInit(){
      this.getBarchartData(this.year)
    }
}
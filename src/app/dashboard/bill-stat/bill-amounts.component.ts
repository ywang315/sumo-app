import { Component, OnInit} from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { WorkingYearService } from '../../local-store/working-year.service';

@Component({
    moduleId: module.id,
    selector: 'bill-stat-amounts',
    templateUrl: 'bill-amounts.component.html',
    styleUrls: ['bill-amounts.component.css']
})
export class BillAmountsComponent implements OnInit{
    months = ["",'JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
    catagories = ['total', 'paid', 'remain']

    errorMessage: string;
    amounts: any;

    constructor(
        private dbService: DashboardService,
        private wys: WorkingYearService
    ){ }

    getAmount(catagory:string, month:string):string{
        if(month == ''){
            return catagory.charAt(0).toUpperCase() + catagory.slice(1);
        }else{
            return this.amounts[catagory][month]
        }
        
    }

    getBillSummaryAmounts(year):any{
        this.dbService.getBillSummaryAmounts(year)
        .subscribe(
            amounts =>{
                this.amounts = amounts
                //console.log(JSON.stringify(amounts['total']['JAN']))
            },
            error => this.errorMessage = <any>error
            
        )
    }

    ngOnInit(){
        this.getBillSummaryAmounts(this.wys.getYear())
    }
}
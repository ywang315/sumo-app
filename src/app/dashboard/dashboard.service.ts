import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';
import { BillStat } from './bill-stat/bill-stat';

@Injectable()
export  class DashboardService {
    private url = HOST
    private headers = new Headers({'Content-Type': 'application/json'})
    constructor(private http: AuthHttp){ };






    getBillSumary(year: string): Observable<BillStat>{
    return this.http.get(`${this.url}/bill-summary/${year}`)
                    .map(
                        res => {
                            let body = <BillStat>res.json()
                            return body || {}
                        }
                    )
                    .catch(this.handleError);
  }

  getBarchartData(year: string):Observable<any[]>{
return this.http.get(`${this.url}/bill-summary/${year}/barchart`)
                      .map(
                          res => {
                              let body = <any[]> res.json()
                              return body || []
                          }
                      )
                      .catch(this.handleError)
  }

  getBillSummaryAmounts(year:string): Observable<any>{
      return this.http.get(`${this.url}/bill-summary/${year}/amounts`)
                      .map(
                          res => {
                              let body = <any> res.json()
                              return body || {}
                          }
                      )
                      .catch(this.handleError)
  }


  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
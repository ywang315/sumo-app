import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GSECCodeComponent } from './gseccodes/gseccode.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewComponent } from './new/new.component';
import { AuthGuard } from './auth/auth.guard';
import { VendorReportComponent } from './vendors/vendor-report.component';
import { VendorReportViewComponent } from './vendors/vendor-report-view.component';
import { VendorListComponent } from './vendors/vendor-list/vendor-list.component';
import { VendorFormComponent } from './vendors/vendor-form.component';

const appRoutes: Routes = [

      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      },
      {
        path: 'new',
        component: NewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'accounts',
        component: GSECCodeComponent,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'vendor-report',
        component: VendorReportViewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'vendors',
        component: VendorListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'vendors/:Shortcut',
        component: VendorFormComponent,
        canActivate: [AuthGuard]
      },


];


@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
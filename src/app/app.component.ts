import { ChangeDetectorRef, Component} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { AuthService } from './auth/auth.service';
import { User } from './auth/user';
//import { MdIconRegistry, MdGridList } from '@angular/material';
import {MatIconRegistry} from '@angular/material';

import { DomSanitizer } from '@angular/platform-browser';
import './rxjs-operator';



@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  viewProviders: [ MatIconRegistry ]

})
export class AppComponent{ 
  user: User = JSON.parse(localStorage.getItem('currentUser'));
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  constructor(public auth: AuthService,
              private mdIconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer,
              changeDetectorRef: ChangeDetectorRef, 
              media: MediaMatcher ){
  mdIconRegistry.addSvgIcon('logout',sanitizer.bypassSecurityTrustResourceUrl('../assets/ic_exit_to_app_black_24px.svg'));
  mdIconRegistry.addSvgIcon('profile',sanitizer.bypassSecurityTrustResourceUrl('../assets/ic_person_black_24px.svg'));
  this.mobileQuery = media.matchMedia('(max-width: 600px)');
  this._mobileQueryListener = () => changeDetectorRef.detectChanges();
  this.mobileQuery.addListener(this._mobileQueryListener);   
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener)
  }

 


 }

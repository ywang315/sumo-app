import { User } from './user';
import { Router } from '@angular/router';
import { Injectable , OnInit} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { HOST } from '../host';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';
import { AuthHttp} from 'angular2-jwt';
import { JwtHelper } from 'angular2-jwt';


@Injectable()
export class AuthService implements OnInit {
    private loggedin:boolean;
    private authUrl = HOST
    private headers = new Headers({'Content-Type':'application/json'});
    public token: string;
    public email: string;
    public user: User = JSON.parse(localStorage.getItem('currentUser'))

    jwtHelper: JwtHelper = new JwtHelper();

    constructor(private http: Http,
                private auth: AuthHttp,
                private router: Router){
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
        
        this.user = new User();
    }

    login(email: string, password: string): Observable<User> {
        let login_headers = new Headers({'Content-Type':'application/json'})
        login_headers.append('Authorization', 'Basic ' + btoa(email+":"+password))
        let url = `${this.authUrl}/token`
         
        
        return this.http.get(url, {headers:login_headers})
        .map(
            res => {
                let body = res.json()
                localStorage.setItem('token', body.token)
                let user = this.jwtHelper.decodeToken(body.token)
                this.user = user;
                localStorage.setItem('currentUser', JSON.stringify(user))
                this.loggedin = true;

                let now = new Date()
                let expiration = new Date(now.getTime() + (parseInt(body.expiration) * 1000))

                localStorage.setItem('expiration', JSON.stringify(expiration))

                return user
            }
        )
        .catch(this.handleError)
        
    }

    register(user:User): Observable<User> {
        let url = `${this.authUrl}/register`
        return this.http.post(url,JSON.stringify(user), {headers: this.headers})
        .map(res => {
            let body = <User>res.json();
            return body || {};
        })
        .catch(this.handleError)
    }


    loggedIn(){
        
        let expiration = new Date(JSON.parse(localStorage.getItem('expiration')))
        let now = new Date()
        let user = this.user = JSON.parse(localStorage.getItem('currentUser'))
        let token = localStorage.getItem('token')

       
        if (token == null || expiration == null || user == null){
            return false;
        } else if (now > expiration){
            return false;
        } else{
            return true;
        }
        
    }
    logout(){
        localStorage.removeItem('token')
        localStorage.removeItem('currentUser')
        this.router.navigate(['/login'])
    }


    private handleError (error: Response | any) {
        //we might use a remote logging infrastructure
        localStorage.removeItem('token')
        localStorage.removeItem('currentUser')
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    } 

    ngOnInit(){
       
        
    }

}
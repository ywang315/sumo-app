export class User{
    constructor(
        public ID?: string,
        public Email?: string,
        public UserName?: string,
        public Password?: string,
        public Created?: Date,
        public isVarified?: boolean,
        public role_id?: number
    ){ }
}
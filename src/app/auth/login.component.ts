import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from './user';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit{
    @Input() user:User;
    errorMessage:string;
    constructor( private auth: AuthService,
                 private router: Router ){ }



    login(){
        
        this.auth.login(this.user.Email,this.user.Password)
        .subscribe(
            user => {
                this.user = user
                this.router.navigate(['/dashboard'])
            },
            error => {
                if(error.indexOf('401') >=0){
                    this.errorMessage = "Authentication Error"
                }else{
                    this.errorMessage = "Network Error. Is your VPN connected?"
                }

                }
        )
    }
   

    ngOnInit(){
        this.user = new User();
    }


}
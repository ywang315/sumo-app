import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from './user';

@Component({
    moduleId: module.id,
    selector: 'register',
    templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
    message: string;
    pwError: string;
    errorMessage:string;
    matchPassword: string;
    user: User;

    constructor(private auth: AuthService, 
                private location: Location,
                private router: Router) { }

    validatePw(){
        if (this.user.Password === this.matchPassword){
            this.pwError = ''
            return true;
        } else {
            this.pwError = 'Passwords donot match'
            return false;
        }
    }

    register():void{
        this.auth.register(this.user)
        .subscribe(
            user => {
                this.message = "Created/Waiting for comfirmation"
                this.router.navigate(['/login'])
            },
            error => this.errorMessage = "Login Error"
        )
    }
    goBack(){
        this.location.back();
    }


    ngOnInit(){
        this.user = new User();
    }

}
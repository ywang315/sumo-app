import { Component, OnInit, Input} from '@angular/core';
import { Contact } from './contact';
import { Address } from './address';
import { Phone } from './phone';
import { Email } from './email';
import { PhoneFormComponent } from './phone-form.component';


@Component({
    moduleId: module.id,
    selector: 'contact-detail',
    templateUrl: 'contact-detail.component.html'
})
export class ContactDetailComponent implements OnInit {
    
    @Input() contact: Contact; 



   

    addEmail(){
        this.contact.Emails.push(new Email())
    }

    deleteEmail(index: number){
        this.contact.Emails.splice(index, 1)
    }

    addPhone(){
        this.contact.Phones.push(new Phone());
    }
    deletePhone(index: number){
        this.contact.Phones.splice(index, 1)
    }

    ngOnInit(){
        //this.contact.Addresses.push(new Address())
        //this.contact.Emails.push(new Email())
        //this.contact.Phones.push(new Phone());
    }
}
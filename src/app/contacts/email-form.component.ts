import { Component, OnInit, Input } from '@angular/core';
import { Email } from './email';

@Component({
    moduleId: module.id,
    selector: 'email-form',
    template: `
        <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label for="Email">Email:</label>
                    </div>

                    <div class="col-md-6">
                        <input type="email" class="form-control" id="Email" name="Email" [(ngModel)]="email.Email">
                    </div>

                    <div class="col-md-3">
                        <input type="text" class="form-control" id="Type" name="Type"
                        [(ngModel)]="email.Type" placeholder="type">
                    </div>
                </div>
            </div>
        </form>

    `
})
export class EmailFormComponent implements OnInit {
    @Input() email: Email;

    ngOnInit(){

    }
}
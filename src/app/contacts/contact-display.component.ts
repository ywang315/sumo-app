import { Component , Input, OnInit } from "@angular/core";
import { Contact } from "./contact";


@Component({
    moduleId: module.id,
    templateUrl: "contact-display.component.html",
    styleUrls: ["contact-display.component.css"],
    selector: "contact-display"
})
export class ContactDisplayComponent implements OnInit{

    @Input() contact: Contact;

    ngOnInit(){
    }

}
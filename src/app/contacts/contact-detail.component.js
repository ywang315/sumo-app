"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var contact_1 = require('./contact');
var phone_1 = require('./phone');
var email_1 = require('./email');
var ContactDetailComponent = (function () {
    function ContactDetailComponent() {
    }
    ContactDetailComponent.prototype.addEmail = function () {
        this.contact.Emails.push(new email_1.Email());
    };
    ContactDetailComponent.prototype.deleteEmail = function (index) {
        this.contact.Emails.splice(index, 1);
    };
    ContactDetailComponent.prototype.addPhone = function () {
        this.contact.Phones.push(new phone_1.Phone());
    };
    ContactDetailComponent.prototype.deletePhone = function (index) {
        this.contact.Phones.splice(index, 1);
    };
    ContactDetailComponent.prototype.ngOnInit = function () {
        //this.contact.Addresses.push(new Address())
        //this.contact.Emails.push(new Email())
        //this.contact.Phones.push(new Phone());
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', contact_1.Contact)
    ], ContactDetailComponent.prototype, "contact", void 0);
    ContactDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'contact-detail',
            templateUrl: 'contact-detail.component.html'
        }), 
        __metadata('design:paramtypes', [])
    ], ContactDetailComponent);
    return ContactDetailComponent;
}());
exports.ContactDetailComponent = ContactDetailComponent;
//# sourceMappingURL=contact-detail.component.js.map
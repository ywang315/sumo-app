export class Phone {
    constructor(
        public Number?: string,
        public Type?: string,
        public PersonOid?: string,
        public Oid?:string,
        public Created?: Date
    ){ }
}
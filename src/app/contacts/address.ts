export class Address {
    constructor(
        public AddressType?: string,
        public Street?: string,
        public Unit?: string,
        public City?: string,
        public ZipCode?: number | string,
        public State?: string,
        public Country?: string,
        public Oid?: string,
        public Created?: Date,
    ) {
        this.Country ="The United States";
    }
}
export class Email{
    constructor(
        public Email?: string,
        public Type?: string,
        public PersonOid?: string,
        public Oid?: string,
        public Created?: Date
    ){ }
}
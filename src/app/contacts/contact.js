"use strict";
var Contact = (function () {
    function Contact(FirstName, LastName, Title, Phones, Emails, Addresses, Oid, Created) {
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Title = Title;
        this.Phones = Phones;
        this.Emails = Emails;
        this.Addresses = Addresses;
        this.Oid = Oid;
        this.Created = Created;
        this.Phones = [];
        this.Emails = [];
        this.Addresses = [];
    }
    return Contact;
}());
exports.Contact = Contact;
//# sourceMappingURL=contact.js.map
import { Phone } from './phone';
import { Address } from './address';
import { Email } from './email';

export class Contact{
    constructor(
        public FirstName?: string,
        public LastName?: string,
        public Title?: string,
        public Phones?: Phone[],
        public Emails?: Email[],
        public Addresses?: Address[],
        public Oid?: string,
        public Created?: Date,
        public TraderOid?: string
    ){ 
        this.Phones = []
        this.Emails = []
        this.Addresses = []
    }
}
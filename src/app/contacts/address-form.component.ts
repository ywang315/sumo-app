import { Component, OnInit, Input } from '@angular/core';
import { Address } from './address';

@Component({
    moduleId: module.id,
    selector: 'address-form',
    templateUrl: 'address-form.component.html'
})
export class AddressFormComponent implements OnInit {
    @Input() address: Address;

    ngOnInit(){
        
    }
}
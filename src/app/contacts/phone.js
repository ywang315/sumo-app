"use strict";
var Phone = (function () {
    function Phone(Number, Type, PersonOid, Oid, Created) {
        this.Number = Number;
        this.Type = Type;
        this.PersonOid = PersonOid;
        this.Oid = Oid;
        this.Created = Created;
    }
    return Phone;
}());
exports.Phone = Phone;
//# sourceMappingURL=phone.js.map
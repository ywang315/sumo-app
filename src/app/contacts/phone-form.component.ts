import { Component, OnInit, Input } from '@angular/core';
import { Phone } from './phone';

@Component({
    moduleId: module.id,
    selector: 'phone-form',
    template: `

        <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label for="Number">Number: </label>
                    </div>

                    <div class="col-md-6">
                        <input type="phone" class="form-control" id="Number" name="Number" [(ngModel)]="phone.Number">
                    </div>
    
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="Type" name="Type" 
                        [(ngModel)]="phone.Type" placeholder="type">
                    </div>
                </div>
            </div>
        </form>

    
    `
})
export class PhoneFormComponent implements OnInit {
    @Input() phone: Phone;

    ngOnInit(){
        //this.phone = new Phone();
    }
}
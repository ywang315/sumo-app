import { Contact } from './contact';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';


@Injectable()
export class ContactsService{
    private url = `${HOST}/contacts`
    private headers = new Headers({'Content-Type':'application/json'});
    constructor(private http: AuthHttp){ };

    delete(contact: Contact): Observable<Contact>{
        let url = `${this.url}/${contact.Oid}`
        return this.http.delete(url, {headers: this.headers}).map(
            res => {
                let body =<Contact>res.json();
                return body || {}
            }
        ).catch(this.handleError)
    }


    private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
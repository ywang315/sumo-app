import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AddressFormComponent } from './address-form.component';
import { ContactDetailComponent } from './contact-detail.component';
import { EmailFormComponent } from './email-form.component';
import { PhoneFormComponent } from './phone-form.component';
import { ContactsService } from './contacts.service';
import { ContactDisplayComponent } from './contact-display.component';

 
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        EmailFormComponent,
        PhoneFormComponent,
        ContactDetailComponent,
        AddressFormComponent,
        ContactDisplayComponent
        
    ],
    exports: [
        AddressFormComponent,
        ContactDetailComponent,
        ContactDisplayComponent
    ],
    providers: [
        ContactsService
    ]
})
export class ContactsModule { }


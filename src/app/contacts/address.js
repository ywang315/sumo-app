"use strict";
var Address = (function () {
    function Address(AddressType, Street, Unit, City, ZipCode, State, Country, Oid, Created) {
        this.AddressType = AddressType;
        this.Street = Street;
        this.Unit = Unit;
        this.City = City;
        this.ZipCode = ZipCode;
        this.State = State;
        this.Country = Country;
        this.Oid = Oid;
        this.Created = Created;
        this.Country = "The United States";
    }
    return Address;
}());
exports.Address = Address;
//# sourceMappingURL=address.js.map
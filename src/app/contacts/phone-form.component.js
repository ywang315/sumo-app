"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var phone_1 = require('./phone');
var PhoneFormComponent = (function () {
    function PhoneFormComponent() {
    }
    PhoneFormComponent.prototype.ngOnInit = function () {
        //this.phone = new Phone();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', phone_1.Phone)
    ], PhoneFormComponent.prototype, "phone", void 0);
    PhoneFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'phone-form',
            template: "\n\n        <form>\n            <div class=\"row\">\n                <div class=\"col-md-12\">\n                    <div class=\"col-md-3\">\n                        <label for=\"Number\">Number: </label>\n                    </div>\n\n                    <div class=\"col-md-6\">\n                        <input type=\"phone\" class=\"form-control\" id=\"Number\" name=\"Number\" [(ngModel)]=\"phone.Number\">\n                    </div>\n    \n                    <div class=\"col-md-3\">\n                        <input type=\"text\" class=\"form-control\" id=\"Type\" name=\"Type\" \n                        [(ngModel)]=\"phone.Type\" placeholder=\"type\">\n                    </div>\n                </div>\n            </div>\n        </form>\n\n    \n    "
        }), 
        __metadata('design:paramtypes', [])
    ], PhoneFormComponent);
    return PhoneFormComponent;
}());
exports.PhoneFormComponent = PhoneFormComponent;
//# sourceMappingURL=phone-form.component.js.map
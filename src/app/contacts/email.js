"use strict";
var Email = (function () {
    function Email(Email, Type, PersonOid, Oid, Created) {
        this.Email = Email;
        this.Type = Type;
        this.PersonOid = PersonOid;
        this.Oid = Oid;
        this.Created = Created;
    }
    return Email;
}());
exports.Email = Email;
//# sourceMappingURL=email.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var app_component_1 = require('./app.component');
var vendor_component_1 = require('./vendors/vendor.component');
var logger_service_1 = require('./logger.service');
var vendor_service_1 = require('./vendors/vendor.service');
var vendor_form_component_1 = require('./vendors/vendor-form.component');
var dashboard_component_1 = require('./dashboard/dashboard.component');
var gseccode_service_1 = require('./gseccodes/gseccode.service');
var breakdown_service_1 = require('./breakdowns/breakdown.service');
var breakdown_component_1 = require('./breakdowns/breakdown.component');
var cash_card_service_1 = require('./cashcard/cash-card.service');
var cash_card_component_1 = require('./cashcard/cash-card.component');
var app_routing_module_1 = require('./app-routing.module');
var bills_module_1 = require('./bills/bills.module');
var new_component_1 = require('./new/new.component');
var new_routing_module_1 = require('./new/new-routing.module');
var contact_detail_component_1 = require('./contacts/contact-detail.component');
var phone_form_component_1 = require('./contacts/phone-form.component');
var email_form_component_1 = require('./contacts/email-form.component');
var address_form_component_1 = require('./contacts/address-form.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                new_routing_module_1.NewRoutingModule,
                bills_module_1.BillsModule,
                app_routing_module_1.AppRoutingModule
            ],
            declarations: [
                app_component_1.AppComponent,
                dashboard_component_1.DashboardComponent,
                vendor_component_1.VendorComponent,
                vendor_form_component_1.VendorFormComponent,
                breakdown_component_1.BreakdownComponent,
                cash_card_component_1.CashCardComponent,
                new_component_1.NewComponent,
                contact_detail_component_1.ContactDetailComponent,
                phone_form_component_1.PhoneFormComponent,
                email_form_component_1.EmailFormComponent,
                address_form_component_1.AddressFormComponent
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [logger_service_1.Logger, vendor_service_1.VendorService, gseccode_service_1.GSECCodeService, breakdown_service_1.BreakdownService, cash_card_service_1.CashCardService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map
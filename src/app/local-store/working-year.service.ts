import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()
export class WorkingYearService{

    setYear(year: number): boolean {
        var y: number = new Date().getFullYear();
        if (typeof year === 'number'){
            y = year;
        }

        try {
            localStorage.setItem('year', JSON.stringify(y))
            return true;
        }
        catch (e) {
            if(e == 'QUOTA_EXCEEDED_ERR'){
                alert("Quota exceeded!");
            }
            this.handleError(e);
            return false;
        }
    }

    getYear(): number {
        try{
            var year = + localStorage.getItem('year');
            if (year == 0){
                this.setYear(new Date().getFullYear());
                return new Date().getFullYear();
            } else {
                return year;
            }
        }
        catch(e){
            this.handleError(e)
            return new Date().getFullYear()
        }
    }




    private handleError (error: Response | any) {
        // Could also use remote logging 
        let errMsg: string;
        if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
      }    
}
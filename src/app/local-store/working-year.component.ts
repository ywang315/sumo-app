import { Component, Input ,OnInit} from '@angular/core';
import { WorkingYearService } from './working-year.service';



@Component({
    moduleId: module.id,
    selector: 'working-year',
    templateUrl: 'working-year.component.html',
    styleUrls: ['working-year.component.css']
})
export class WorkingYearComponent implements OnInit{
    year: number;
    constructor(private wys: WorkingYearService){
    }

    updateWorkingYear(delta: number){
        var updated = this.wys.setYear(this.year + delta)
        if (updated) {
            this.year = this.year + delta
        }

    }

    ngOnInit(){
        this.year = this.wys.getYear();
    }
}
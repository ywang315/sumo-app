import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { WorkingYearService } from './working-year.service';
import { WorkingYearComponent } from './working-year.component';
//import {MaterialModule, MdButtonModule, MdCheckboxModule, MdGridListModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';

@NgModule({
    imports: [
        MatCardModule,
        FormsModule,
        CommonModule,
        HttpModule,
        //MaterialModule, 
        //MdButtonModule, 
        //MdCheckboxModule, 
        //MdGridListModule
    ],
    declarations: [
        WorkingYearComponent
    ],
    providers: [
        WorkingYearService
    ],
    exports: [
        WorkingYearComponent
    ]
})
export class LocalStoreModule { }
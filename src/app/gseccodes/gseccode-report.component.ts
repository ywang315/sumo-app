import { Component, Input, OnInit } from '@angular/core';
import { GSECCode } from './gseccode';
import { GSECCodeService } from './gseccode.service';
import { ReportItem } from './reportitem';
//import { MdDialog } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UpdateEstimateService } from './update-estimate.service';
import { MonthlyExpensesService } from '../monthlyexpenses/monthlyexpenses.service';
import { HOST } from '../host';
import 'rxjs/Rx';
import * as FileSaver from 'file-saver'
import { WorkingYearService } from '../local-store/working-year.service';


@Component({
    moduleId: module.id,
    templateUrl: 'gseccode-report.component.html',
    styleUrls: ['gseccode-report.component.css'],
    selector: 'gsec-report'
})
export class GSECCodeReportComponent{

    @Input() code: string;
    @Input() year: string;
    errorMessage: string;
    position: string = "above"
    downloadUrl: string;
    filename: string;

    reportItems: ReportItem[] = []
    estimate: ReportItem;
    difference: ReportItem;


    constructor(
        private gseccodeService: GSECCodeService,
        private updateEstimateService: UpdateEstimateService,
        private mespService: MonthlyExpensesService,
        private wys: WorkingYearService
    ) { }

    
    updateEstimate(month: string){
        this.updateEstimateService.UpdateEstimate(this.year,month, this.code)
        .subscribe(res => {
            if(res != undefined){
                this.doUpdateEstimate(res)
            }
        })
    }
    

    doUpdateEstimate(line: string){
        this.mespService.updateEstimate(line).subscribe(
            res => {
                this.getReport()
            },
            error => this.errorMessage = <any> error
        )
    }

    getReport(){
        this.downloadUrl = `${HOST}/gseccodes/${this.code}/recon/${this.year}/report`
        this.gseccodeService.getReport(this.year, this.code)
        .subscribe( report => {
            this.reportItems = report;
            this.estimate = report[report.length - 1];
            this.difference = report[report.length - 2];
        },
        error => this.errorMessage = <any> error
        )
        
    }
    downloadFile(data: Response) {
        var blob = new Blob([data], {type: "application/vnd.ms-excel"})
        
        FileSaver.saveAs(blob, `${this.code}_${this.year}.xlsx`)
        
    }
    downloadRepodrt(){
        this.gseccodeService.downloadReport(this.downloadUrl).subscribe(
            data => {
                this.downloadFile(data)
            },
            error => this.errorMessage = <any>error
        )
 
    }
    ngOnInit(){
        if (this.code != undefined){
            this.getReport();
        }
    }

}
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
//import { MaterialModule } from '@angular/material';
//import { MdCardModule } from '@angular/material';

import { GSECCodeService } from './/gseccode.service';
import { GSECCodeComponent } from './gseccode.component';
import { GSECCodeFormComponent } from './gseccode-form.component';
import { GSECCodeReportComponent } from './gseccode-report.component';
import { LocalStoreModule } from '../local-store/local-store.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';


@NgModule({
    imports: [
        MatTooltipModule,
        MatSelectModule,
        MatCardModule,
        NoopAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        BrowserAnimationsModule,
        MatDialogModule,
        //MaterialModule,
        //MdCardModule,
        CommonModule,
        FormsModule,
        LocalStoreModule
    ],
    declarations: [
        GSECCodeFormComponent,
        GSECCodeComponent,
        GSECCodeReportComponent
    ],
    providers: [
        GSECCodeService
    ],
    exports: [
        GSECCodeFormComponent,
        GSECCodeComponent,
        GSECCodeReportComponent
    ]
})
export class GSECCodeModule { }
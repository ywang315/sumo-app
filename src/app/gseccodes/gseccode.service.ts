import { GSECCode } from './gseccode';
import { ReportItem } from './reportitem';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';
import { ResponseContentType } from '@angular/http';



@Injectable()
export class GSECCodeService {
    private gseccodeUrl = `${HOST}/gseccodes`
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: AuthHttp) { };

    getGSECCodes(): Observable<GSECCode[]>{
        return this.http.get(this.gseccodeUrl)
                        .map( res => {
                            let body = res.json()
                            return body || []
                        })
                        .catch(this.handleError)
    }

    getGSECCodelist(): Observable<string[]>{
      let url = `${HOST}/gseccodelist`
        return this.http.get(url)
                        .map( res => {
                            let body = <string[]>res.json()
                            return body || []
                        })
                        .catch(this.handleError)
    }
    
    getReport(year: string, code: string): Observable<string[]> {
      let url = `${this.gseccodeUrl}/${code}/recon/${year}`

      return this.http.get(url)
                      .map( res => {
                        let body = <string[]> res.json()
                        return body || []
                      })
                      .catch(this.handleError)
    }

    downloadReport(url: string): Observable<any>{
      let headers = new Headers({
        'Content-Type': 'application/json',
        'Accept' : 'application/vnd.ms-excel'
      })
      return this.http.get(url, {
        headers: headers,
        responseType: ResponseContentType.ArrayBuffer
      })
       .map(res => res['_body'])
      .catch(this.handleError)
    }




  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
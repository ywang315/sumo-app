import { GSECCode } from './gseccode';
import { Component, OnInit, Input } from '@angular/core'; 
import { GSECCodeService } from './gseccode.service';


@Component({
    moduleId: module.id,
    selector: 'gseccode-form',
    templateUrl: 'gseccode-form.component.html'
})
export class GSECCodeFormComponent {
    @Input() gseccode: GSECCode
    @Input() edit: boolean
}

import { UpdateEstimateDialog } from './update-estimate.dialog';
//import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Injectable()
export class UpdateEstimateService {
    
    constructor(private dialog: MatDialog){

    }


    public UpdateEstimate(year:string, month:string, code:string){
        //let dialogRef: MatDialogRef<UpdateEstimateDialog>
        let dialogRef = this.dialog.open(
            UpdateEstimateDialog,
            {   
                width: '250px',
                data: {
                    title: "Update Monthly Estimate",
                    message: "Are you sure you want to update the estimate amount?",
                    year: year,
                    month: month,
                    code: code
                }
            })
            return dialogRef.afterClosed()


        /*
        dialogRef = this.dialog.open(UpdateEstimateDialog);
        dialogRef.componentInstance.title = "Update Monthly Estimate"
        dialogRef.componentInstance.message = "Are you sure you want to update the estimate amount?"
        dialogRef.componentInstance.year = year;
        dialogRef.componentInstance.month = month;
        dialogRef.componentInstance.code = code;

        

        */

    }
    
}
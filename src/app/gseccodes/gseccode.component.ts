import { Component, Input, OnInit } from '@angular/core';
import { GSECCode } from './gseccode';
import { GSECCodeService } from './gseccode.service';
import { ReportItem } from './reportitem';
//import { MdDialog } from '@angular/material';
import { UpdateEstimateService } from './update-estimate.service';
import { MonthlyExpensesService } from '../monthlyexpenses/monthlyexpenses.service';
import { HOST } from '../host';
import 'rxjs/Rx';
import * as FileSaver from 'file-saver'
import { WorkingYearService } from '../local-store/working-year.service';


@Component({
    moduleId: module.id,
    templateUrl: 'gseccode.component.html',
    styleUrls: ['gseccode.component.css'],
})
export class GSECCodeComponent implements OnInit {

    gseccodelist: string[] = []
    errorMessage: string;
    message: string;
    @ Input() selectedAccount: string;
    //year: string = (new Date().getFullYear()) + ""
    year: string = this.wys.getYear() + ""
    reportItems: ReportItem[] = []
    estimate: ReportItem;
    difference: ReportItem;
    position: string = "above"

    downloadUrl: string;
    filename: string;

    constructor(
        private gseccodeService: GSECCodeService,
        private updateEstimateService: UpdateEstimateService,
        private mespService: MonthlyExpensesService,
        private wys: WorkingYearService
    ) { }
    
    updateEstimate(month: string){
        this.updateEstimateService.UpdateEstimate(this.year,month, this.selectedAccount)
        .subscribe(res => {
            if(res != undefined){
                this.doUpdateEstimate(res)
            }
        })
    }
    
    doUpdateEstimate(line: string){
        this.mespService.updateEstimate(line).subscribe(
            res => {
                this.getReport()
            },
            error => this.errorMessage = <any> error
        )
    }



    getReport(){
        this.downloadUrl = `${HOST}/gseccodes/${this.selectedAccount}/recon/${this.year}/report`
        this.gseccodeService.getReport(this.year, this.selectedAccount)
        .subscribe( report => {
            this.reportItems = report;
            this.estimate = report[report.length - 1];
            this.difference = report[report.length - 2];
        },
        error => this.errorMessage = <any> error
        )
        
    }
    
    getListofGSECAccounts(){
        this.gseccodeService.getGSECCodelist().subscribe(
            gseccodes => {
                this.gseccodelist = gseccodes
            },
            error => this.errorMessage = <any> error
        )
    }
    downloadFile(data: Response) {
        var blob = new Blob([data], {type: "application/vnd.ms-excel"})
        
        FileSaver.saveAs(blob, `${this.selectedAccount}_${this.year}.xlsx`)
        
    }

    downloadRepodrt(){
        this.gseccodeService.downloadReport(this.downloadUrl).subscribe(
            data => {
                this.downloadFile(data)
            },
            error => this.errorMessage = <any>error
        )
 
    }
    ngOnInit(){
        this.getListofGSECAccounts()
        if (this.selectedAccount != undefined){
            this.getReport();
        }
    }
}



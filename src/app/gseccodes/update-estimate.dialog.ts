//import { MdDialogRef } from '@angular/material';
import { Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
    selector: 'update-estimate',
    templateUrl: 'update-estimate.dialog.html',
    styleUrls: ['update-estimate.dialog.css']
})
export class UpdateEstimateDialog {
    public title: string;
    public message: string;

    public amount: number;
    public year: string;
    public month: string;
    public code: string;


    
    result(){
        return this.year + "+" + this.month + "+" + this.code + "+" + this.amount
    }

 
    constructor(
        public dialogRef: MatDialogRef<UpdateEstimateDialog>,
        @Inject(MAT_DIALOG_DATA) public data:any) { }
    
        onNoClick(): void {
            this.dialogRef.close();
        }
}

export class GSECCode {
    constructor (
        public Code?: string,
        public AccountType?: string,
        public ChargeOids?: string[],
        public CreditOids?: string[],
        public TraderOid?: string,
        public StartDate?: Date,
        public EndDate?: Date,
        public Created?: Date
    ){
        this.AccountType = "Trader Account"
    }
}
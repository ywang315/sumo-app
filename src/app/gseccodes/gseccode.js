"use strict";
var GSECCode = (function () {
    function GSECCode(Code, AccountType, ChargeOids, CreditOids, TraderOids, StartDate, EndDate, Created) {
        this.Code = Code;
        this.AccountType = AccountType;
        this.ChargeOids = ChargeOids;
        this.CreditOids = CreditOids;
        this.TraderOids = TraderOids;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.Created = Created;
    }
    return GSECCode;
}());
exports.GSECCode = GSECCode;
//# sourceMappingURL=gseccode.js.map
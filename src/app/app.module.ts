import { NgModule , CUSTOM_ELEMENTS_SCHEMA}      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent }  from './app.component';
import { Logger } from './logger.service';
import { RouterModule, Routes } from '@angular/router';
import { BreakdownService } from './breakdowns/breakdown.service';
import { AppRoutingModule } from './app-routing.module';
import { BillsModule } from './bills/bills.module';
import { NewComponent } from './new/new.component';
import { NewRoutingModule } from './new/new-routing.module';
import { TraderFormComponent } from './traders/trader-form.component';
import { TradersModule } from './traders/trader.module'
import { TraderService } from './traders/trader.service'
import { ContactsModule } from './contacts/contacts.module';
import { VendorsModule } from './vendors/vendors.module';
import { AddressFormComponent } from './contacts/address-form.component';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './auth/auth.guard';
//import { MaterialModule } from '@angular/material';
//import { MdIconRegistry } from '@angular/material';
//import { MdGridListModule } from '@angular/material';
//import { MdButtonModule, MdCheckboxModule } from '@angular/material';
import { ConfirmDialogComponent } from './confirm-dialog.component';
import { UpdateEstimateDialog } from './gseccodes/update-estimate.dialog';
import { UpdateEstimateService } from './gseccodes/update-estimate.service';
import { MonthlyExpensesService } from './monthlyexpenses/monthlyexpenses.service';
import { MomentModule } from 'angular2-moment/moment.module';
import "hammerjs";
import { ChartsModule } from 'ng2-charts';
import { DashboardModule } from './dashboard/dashboard.module';
import { NgPipesModule } from 'ngx-pipes';
import { GSECCodeModule } from './gseccodes/gseccode.module';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { MatDialogModule } from '@angular/material';


@NgModule({
  imports: [ 
    MatDialogModule,
    MatListModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    //MaterialModule,
    //MdButtonModule, 
    //MdCheckboxModule,
    //MdGridListModule,
    BrowserModule, 
    FormsModule, 
    HttpModule, 
    NewRoutingModule,
    BillsModule,
    AppRoutingModule,
    ContactsModule,
    VendorsModule,
    AuthModule,
    TradersModule,
    MomentModule,
    ChartsModule,
    DashboardModule,
    NgPipesModule,
    GSECCodeModule
  ],
  declarations: [ 
    AppComponent, 
    NewComponent,
    UpdateEstimateDialog,
    ConfirmDialogComponent
  ],
  bootstrap:    [ AppComponent ],
  providers: [ 
    Logger, 
    BreakdownService, 
    TraderService,
    AuthGuard,
    MonthlyExpensesService,
    UpdateEstimateService
  ],
  exports: [
    UpdateEstimateDialog,
    ConfirmDialogComponent
  ],
  entryComponents: [
    UpdateEstimateDialog,
    ConfirmDialogComponent
  ]

})
export class AppModule { }



//import { map, filter, mergeMap, tap } from 'rxjs/operators';

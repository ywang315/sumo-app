import { Component, Input, OnInit } from '@angular/core';
import { ReportItem } from './reportitem';
import { Report } from "./report";
import { VendorService } from './vendor.service';
import { VendorReportComponent } from './vendor-report.component';
import * as FileSaver from 'file-saver'
import { HOST } from '../host';
import { WorkingYearService } from '../local-store/working-year.service';

@Component({
    moduleId: module.id,
    templateUrl: 'vendor-report-view.component.html',
    styleUrls: ['vendor-report-view.component.css']
})
export class VendorReportViewComponent implements OnInit {
    reports:Report[]

    groups: string[] = [];
    groupedReports: any[] = [];
    errorMessage: string;
    //year: string = new Date().getFullYear() + "";
    year: string = this.wys.getYear() + "";
    showdetails: boolean = true;
    

    constructor(
        private vendorService: VendorService,
        private wys: WorkingYearService
    ){}

    toggleall(flag){
        this.showdetails = flag;
    }

    sortStrFn(r1, r2){
        if(r1<r2){
            return -1;
        }else if(r1>r2){
            return 1;
        }else{
            return 0;
        }
    }
    i = 0;
    groupReports(rps){
        if(this.groups.indexOf(rps.industry) === -1){
            this.groups.push(rps.industry)
            this.groupedReports[rps.industry] = [rps]
        } else{
            this.groupedReports[rps.industry].push(rps)
        }
    }


    getReports(){
        this.vendorService.getAllReports(this.year)
        .subscribe(
            res => {
                this.reports = <Report[]>res,
                (<Report[]>res).forEach(
                    rps=>{
                        this.groupReports(rps)
                    }
                )
                this.groups.sort(this.sortStrFn)

            },
            error => this.errorMessage = error
        )
    }

        downloadFile(data: Response) {
        var blob = new Blob([data], {type: "application/vnd.ms-excel"})
        
        FileSaver.saveAs(blob, `Vendor_Report_${this.year}.xlsx`)
        
    }

    downloadRepodrt(){
        let downloadUrl = `${HOST}/vendors/report/${this.year}/download`
        this.vendorService.downloadReport(downloadUrl).subscribe(
            data => {
                this.downloadFile(data)
            },
            error => this.errorMessage = <any>error
        )
 
    }
    ngOnInit(){
        this.getReports()
    }
}
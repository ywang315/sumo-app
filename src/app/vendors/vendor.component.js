"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var vendor_service_1 = require('./vendor.service');
var core_1 = require('@angular/core');
require('rxjs/add/operator/switchMap');
var bill_1 = require('../bills/bill');
var VendorComponent = (function () {
    function VendorComponent(vendorService) {
        this.vendorService = vendorService;
    }
    ;
    VendorComponent.prototype.getVendor = function (Oid) {
        var _this = this;
        this.vendorService.getVendor(Oid)
            .subscribe(function (vendor) { return _this.vendor = vendor; }, function (error) { return _this.errorMessage = error; });
    };
    VendorComponent.prototype.getVendorByOid = function () {
        var _this = this;
        this.vendorService.getVendor(this.vendorOid)
            .subscribe(function (vendor) { return _this.vendor = vendor; }, function (error) { return _this.errorMessage = error; });
    };
    VendorComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', bill_1.Bill)
    ], VendorComponent.prototype, "bill", void 0);
    VendorComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'vendor',
            templateUrl: 'vendor.component.html'
        }), 
        __metadata('design:paramtypes', [vendor_service_1.VendorService])
    ], VendorComponent);
    return VendorComponent;
}());
exports.VendorComponent = VendorComponent;
//# sourceMappingURL=vendor.component.js.map
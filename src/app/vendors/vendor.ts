import { Contact } from '../contacts/contact';
import { Address } from '../contacts/address';
import { Bill } from '../bills/bill';

export class Vendor {
    constructor(
        public Shortcut?: string,
        public Name?: string,
        public Bills?: Bill[],
        public Contacts? : Contact[],
        public Created? : Date,
        public PaymentInstruction?: string,
        public ServiceSubscribers?: string[],
        public TraderAccounts?: string[],
        public Address?: Address,
        public TIN?: string,
        public Services?: string[] | string
    ) {
        
        this.Bills = new Array<Bill>()
        this.Contacts = new Array<Contact>()
        this.ServiceSubscribers = [];
        this.TraderAccounts = [];
        this.Services = [];
        this.Address = {};
     }


}


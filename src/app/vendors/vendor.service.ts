import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, Response} from '@angular/http';
import { Vendor } from './vendor';
import { Observable } from 'rxjs/Observable'
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';
import { Report } from './report';
import { ResponseContentType } from '@angular/http';
import { ReportItem } from './reportitem';

@Injectable()
export class VendorService {
    private vendorsUrl = `${HOST}/vendors`;
    private headers = new Headers({'Content-Type':'application/json'});

    constructor (private http: AuthHttp){ };


    getVendor(shortcut:string):Observable<Vendor>{
        let url = `${this.vendorsUrl}/${shortcut}`
        return this.http.get(url).map(this.extractData)
                                .catch(this.handleError)
    }
    private extractData(res: Response){
        let body = res.json()
        return body || {};
    }
    getAllReports(year: string): Observable<Report[]>{
        let url = `${this.vendorsUrl}/report/${year}`
        return this.http.get(url).map(
            res => {
                let body = <Report[]> res.json()
                return body
            }
        ).catch(this.handleError)
    }

    getReport(vendor: String, year: string): Observable<ReportItem[]>{
        let url = `${this.vendorsUrl}/${vendor}/report/${year}`
        return this.http.get(url).map(
            res => {
                let body = <ReportItem[]> res.json()
                return body
            }
        ).catch(this.handleError)
    }

    getVendors():Observable<Vendor[]>{
        let url = this.vendorsUrl
        return this.http.get(url).map(
            res => {
                let body = <Vendor[]> res.json()
                return body || []
            }
        ).catch(this.handleError)
    }

    getVendorList():Observable<string[]>{
        let url = `${this.vendorsUrl}?shortcutonly`
        return this.http.get(url)
        .map(res => {let body = <string[]>res.json()
            return body || []})
        .catch(this.handleError)
    }

    create(vendor: Vendor): Observable<Vendor>{
        return this.http
        .post(this.vendorsUrl, JSON.stringify(vendor), {headers: this.headers})
        .map(res => {
            let body = <Vendor>res.json();
            return body || {};
        })
        .catch(this.handleError)
    }

    update(vendor: Vendor): Observable<Vendor>{
        return this.http.put(this.vendorsUrl, JSON.stringify(vendor), {headers: this.headers})
                        .map(res => {
                            let body = <Vendor>res.json()
                            return body || {}
                        })
                        .catch(this.handleError)
    }

    delete(vendor: Vendor): Observable<Vendor>{
        let url = `${this.vendorsUrl}/${vendor.Shortcut}`
        return this.http.delete(url).map(
            res => {
                let body = <Vendor>res.json();
                return body || {};
            }
        ).catch(this.handleError)
    }

    downloadReport(url: string): Observable<any>{
      let headers = new Headers({
        'Content-Type': 'application/json',
        'Accept' : 'application/vnd.ms-excel'
      })
      return this.http.get(url, {
        headers: headers,
        responseType: ResponseContentType.ArrayBuffer
      })
       .map(res => res['_body'])
      .catch(this.handleError)
    }



    



    private handleError (error: Response | any) {
        //we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    } 

}
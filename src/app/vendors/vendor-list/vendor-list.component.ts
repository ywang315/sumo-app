import { Component, OnInit } from '@angular/core';
import { VendorService } from '../vendor.service';
import { Vendor } from '../vendor';
import { VendorItemComponent } from './vendor-item.component';

@Component({
    moduleId: module.id,
    templateUrl: 'vendor-list.component.html',
    styleUrls: ['vendor-list.component.css']
})
export class VendorListComponent implements OnInit{
    errorMessage:string
    vendors: Vendor[]

    constructor(
        private vendorService: VendorService,
    ){}

    getVendors(){
        this.vendorService.getVendors().subscribe(
            vendors => this.vendors = vendors,
            error => this.errorMessage = <any> error
        )
    }
    

    ngOnInit(){
        this.getVendors()
    }
}
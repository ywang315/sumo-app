import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Vendor } from '../vendor';
import { VendorService } from '../vendor.service';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    templateUrl: 'vendor-item.component.html',
    styleUrls: ['vendor-item.component.css'],
    selector: 'vendor-item'
})
export class VendorItemComponent{
    @Input()
    vendor: Vendor

    errorMessage: string;
    
    canDelete(){
        if(this.vendor != undefined){
            return this.vendor.Bills.length == 0
        } else {
            return false
        }
    }
    constructor(
        private vendorService: VendorService,
        public snackBar: MatSnackBar,
        private router: Router

    ){}

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
        duration: 4000,
        });
    }
    editVendor(Shortcut: string){
        let path = `/vendors/${Shortcut}`
        this.router.navigate([path])
    }


    @Output() confirmDeletion: EventEmitter<Vendor> = new EventEmitter<Vendor>();
    delete(){
        this.vendorService.delete(this.vendor).subscribe(
            vendor => {
                console.log("deleted: " + vendor.Shortcut)
                this.confirmDeletion.emit(vendor)
                this.openSnackBar(vendor.Shortcut + " is deleted", "DELETED!")
            },
            error => {
                this.errorMessage = <any>error
                console.log(error)
                this.openSnackBar(error, "Not Deleted!")
            }
        )
    }
}
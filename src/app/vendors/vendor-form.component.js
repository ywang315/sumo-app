"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var vendor_service_1 = require('./vendor.service');
var vendor_1 = require('./vendor');
var core_1 = require('@angular/core');
require('rxjs/add/operator/switchMap');
var contact_1 = require('../contacts/contact');
var address_1 = require('../contacts/address');
var phone_1 = require('../contacts/phone');
var email_1 = require('../contacts/email');
var VendorFormComponent = (function () {
    function VendorFormComponent(vendorService) {
        this.vendorService = vendorService;
    }
    VendorFormComponent.prototype.toServicesList = function () {
        this.vendor.Services = this.servicesString.split(',').map(function (item) { return item.trim(); });
        console.log(JSON.stringify(this.vendor.Services));
    };
    VendorFormComponent.prototype.addContact = function () {
        var contact = new contact_1.Contact();
        contact.Addresses.push(new address_1.Address());
        contact.Emails.push(new email_1.Email());
        contact.Phones.push(new phone_1.Phone());
        this.vendor.Contacts.push(contact);
    };
    VendorFormComponent.prototype.toString = function () {
        console.log(JSON.stringify(this.vendor));
    };
    VendorFormComponent.prototype.save = function () {
        var _this = this;
        this.vendorService.create(this.vendor)
            .subscribe(function (vendor) {
            _this.vendor = vendor;
            _this.message = "Created";
            console.log("created");
        }, function (error) {
            _this.message = "Not Created";
            _this.errorMessage = error;
        });
    };
    VendorFormComponent.prototype.deleteContact = function (index) {
        this.vendor.Contacts.splice(index, 1);
    };
    VendorFormComponent.prototype.ngOnInit = function () {
        this.vendor = new vendor_1.Vendor();
        this.vendor.Address = new address_1.Address();
        this.vendor.Contacts = new Array();
        var contact = new contact_1.Contact();
        contact.Addresses.push(new address_1.Address());
        contact.Emails.push(new email_1.Email());
        contact.Phones.push(new phone_1.Phone());
        this.vendor.Contacts.push(contact);
    };
    VendorFormComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'vendor-form',
            templateUrl: 'vendor-form.component.html'
        }), 
        __metadata('design:paramtypes', [vendor_service_1.VendorService])
    ], VendorFormComponent);
    return VendorFormComponent;
}());
exports.VendorFormComponent = VendorFormComponent;
//# sourceMappingURL=vendor-form.component.js.map
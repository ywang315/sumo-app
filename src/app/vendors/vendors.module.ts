import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ContactsModule } from '../contacts/contacts.module';

import { VendorFormComponent } from './vendor-form.component';
import { VendorComponent } from './vendor.component';
import { VendorService } from './vendor.service';
import { VendorReportComponent } from './vendor-report.component';
import { VendorReportViewComponent } from './vendor-report-view.component';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { VendorItemComponent } from './vendor-list/vendor-item.component';
//import { MaterialModule } from '@angular/material';
import { ListToStringPipe } from './list-to-string-pipe.component';
import { LocalStoreModule } from '../local-store/local-store.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatChipsModule} from '@angular/material/chips';


@NgModule({
    imports: [
        MatChipsModule,
        MatToolbarModule,
        MatCardModule,
        MatButtonModule,
        MatSnackBarModule,
        ContactsModule,
        CommonModule,
        FormsModule,
        ContactsModule,
        //MaterialModule,
        LocalStoreModule
    ],
    declarations: [
        VendorComponent,
        VendorFormComponent,
        VendorReportComponent,
        VendorReportViewComponent,
        VendorListComponent,
        VendorItemComponent,
        ListToStringPipe
    ],
    providers: [ VendorService ],
    schemas: [
        
    ]
})
export class VendorsModule { }

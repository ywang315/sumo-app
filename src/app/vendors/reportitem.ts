export class ReportItem {
    constructor(
        public name?: string,
        public JAN?: string,
        public FEB?: string,
        public MAR?: string,
        public APR?: string,
        public MAY?: string,
        public JUN?: string,
        public JUL?: string,
        public AUG?: string,
        public SEP?: string,
        public OCT?: string,
        public NOV?: string,
        public DEC?: string
    ){}

}


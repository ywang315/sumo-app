import { Component, Input, OnInit } from '@angular/core';
import { ReportItem } from './reportitem';
import { VendorService } from './vendor.service';
import { Router } from '@angular/router';
import { WorkingYearService } from '../local-store/working-year.service';

@Component({
    moduleId: module.id,
    selector: 'vendor-report-item',
    templateUrl: 'vendor-report.component.html',
    styleUrls: ['vendor-report.component.css']
})
export class VendorReportComponent implements OnInit{


    venderList: string[];
    errorMessage: string;
    //@Input() year: string = new Date().getFullYear() + ""
    @Input() year: string = this.wys.getYear() + ""
    @Input() reportItems: ReportItem[];
    @Input() vendor: string;
    selectedVendor: string;
    @Input() show:boolean = true;
    

    constructor(
        private vendorService: VendorService,
        private router: Router,
        private wys: WorkingYearService
    ){}

    toggle(flag){
        this.show = flag
    }

    getReport(){
        this.vendorService.getReport(this.selectedVendor, this.year)
        .subscribe( report => {
            this.reportItems = report
        },
        error => this.errorMessage = <any>error)
    }

    getVendorList(){
        this.vendorService.getVendorList().subscribe(
            vendors => {
                this.venderList = <string[]>vendors
            },
            error => this.errorMessage = error
        )
    }

    goToBill(vendor: string, month:  string, year: string){
        let path = `/bills-by-vendor/?VendorShortcut=${vendor}&Month=${month}&Year=${year}`
        this.router.navigate([path])
    }

    isNumber(item: any):boolean{
        if(isNaN(Number(item))){
            return false
        } else {
            return true
        }
    }

    ngOnInit(){
        //this.getVendorList()

    }
}
import { VendorService } from './vendor.service';
import { Vendor } from './vendor';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { Bill } from '../bills/bill';



@Component({
    moduleId: module.id,
    selector: 'vendor',
    templateUrl: 'vendor.component.html'
})
export class VendorComponent implements OnInit {
        @Input() bill: Bill;
        vendorUrl: string;
        vendorOid: string;
        vendor:Vendor;
        errorMessage: string;

    constructor(
        private vendorService : VendorService
        ){ };
    
    getVendor(Oid:string){
        this.vendorService.getVendor(Oid)
        .subscribe(
            vendor => this.vendor = vendor,
            error => this.errorMessage = <any>error
        )
    }
    getVendorByOid(){
         this.vendorService.getVendor(this.vendorOid)
        .subscribe(
            vendor => this.vendor = vendor,
            error => this.errorMessage = <any>error
        )
    }


 

    ngOnInit(){
        
    }
}


import { ReportItem } from "./reportitem";

export class Report {
    constructor(
        public vendor?: string,
        public industry?: string,
        public report?: ReportItem[]
    ) {}
}




import { VendorService }  from './vendor.service';
import { Vendor } from './vendor';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router} from '@angular/router';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { ContactDetailComponent } from '../contacts/contact-detail.component';
import { AddressFormComponent } from '../contacts/address-form.component';
import { Contact } from '../contacts/contact';
import { Address } from '../contacts/address';
import { Phone } from '../contacts/phone';
import { Email } from '../contacts/email';
import { ListToStringPipe } from './list-to-string-pipe.component';
import { ContactsService } from '../contacts/contacts.service';

@Component({
    moduleId: module.id,
    selector: 'vendor-form',
    templateUrl: 'vendor-form.component.html'
})
export class VendorFormComponent implements OnInit{
    message: string;
    errorMessage: string;
    @Input() vendor: Vendor;
    @Input() edit: boolean = false;
    
    servicesString: string;
    contactsToRemove: Contact[] = []

    constructor(
        private vendorService : VendorService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        public snackBar: MatSnackBar,
        private contactService: ContactsService
    ) { }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
        duration: 2000,
        });
    }

    toServicesList(){
        if(typeof this.servicesString != 'undefined'){
            this.vendor.Services = this.servicesString.split(',').map( item => {return item.trim()})  
            this.vendor.Services.splice(0,0,'')
        }
    }
        
    
    addContact(){
        let contact = new Contact()
        contact.Addresses.push(new Address())
        contact.Emails.push(new Email())
        contact.Phones.push(new Phone())
        this.vendor.Contacts.push(contact)
    }
    toString(){
        console.log(JSON.stringify(this.vendor))
    }

    save(){
        this.vendorService.create(this.vendor)
        .subscribe(
            vendor => {
                this.vendor = vendor
                this.message = "Created" 
                this.openSnackBar(vendor.Shortcut, 'Created!')
            },
            error => {
                this.message = "Not Created"
                this.errorMessage = <any>error
                this.openSnackBar(this.errorMessage, 'Not Created!')
            }
        )
    }

    update(){
        this.contactsToRemove.forEach(
            contact => {
                this.contactService.delete(contact).subscribe()
            }
        )
        this.vendorService.update(this.vendor).subscribe(
            vendor => {
                this.vendor = vendor
                this.message = "Updated"
                this.openSnackBar(vendor.Shortcut, 'Updated!')
            },
            error => {
                this.message = 'Not Updated'
                this.errorMessage = <any>error
                this.openSnackBar(this.errorMessage, "Not Updated")
            }
        )
    }

    getVendor(Shortcut:string){
        this.vendorService.getVendor(Shortcut).subscribe(
            vendor => {
                this.vendor = vendor
                this.servicesString = vendor.Services.slice(1,vendor.Services.length).toString()
            },
            error => {
                this.errorMessage = <any>error
            }
        )
    }

    deleteContact(index: number){
        this.contactsToRemove.push(this.vendor.Contacts[index])
        this.vendor.Contacts.splice(index, 1)
    }

    initVendorCreation(){
        this.vendor = new Vendor();
        this.vendor.Address = new Address();
        this.vendor.Contacts = [];

    }


    ngOnInit(){
        let Shortcut = this.route.snapshot.params['Shortcut'];
        if(typeof Shortcut != 'undefined'){
            this.getVendor(Shortcut)
            this.edit = true
        } else {
            this.initVendorCreation()
        }
    }
}
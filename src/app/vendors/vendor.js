"use strict";
var Vendor = (function () {
    function Vendor(Shortcut, Name, Bills, Contacts, Created, PaymentInstruction, ServiceSubscribers, TraderAccounts, Address, TIN, Services) {
        this.Shortcut = Shortcut;
        this.Name = Name;
        this.Bills = Bills;
        this.Contacts = Contacts;
        this.Created = Created;
        this.PaymentInstruction = PaymentInstruction;
        this.ServiceSubscribers = ServiceSubscribers;
        this.TraderAccounts = TraderAccounts;
        this.Address = Address;
        this.TIN = TIN;
        this.Services = Services;
        this.Bills = new Array();
        this.Contacts = new Array();
        this.ServiceSubscribers = [];
        this.TraderAccounts = [];
        this.Services = [];
        this.Address = {};
    }
    return Vendor;
}());
exports.Vendor = Vendor;
//# sourceMappingURL=vendor.js.map
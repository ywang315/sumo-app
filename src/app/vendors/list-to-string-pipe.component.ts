import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'list-to-string'
})
export class ListToStringPipe implements PipeTransform {
    transform(lst: string[]): string{
        let msg = lst.toString()
        return msg
    }
}
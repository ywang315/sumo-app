import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillFormComponent } from '../bills/bill-form.component';
import { NewComponent } from './new.component';
import { VendorFormComponent } from '../vendors/vendor-form.component';
import { TraderFormComponent } from '../traders/trader-form.component';
import { AuthGuard } from '../auth/auth.guard';

const newRoutes: Routes = [
    {
        path: 'new',
        component: NewComponent,
        children: [
            {
                path: 'bill',
                component: BillFormComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'vendor',
                component: VendorFormComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'trader',
                component: TraderFormComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(newRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class NewRoutingModule { }
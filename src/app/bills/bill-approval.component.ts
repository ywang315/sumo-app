import { Component, OnInit, Input } from '@angular/core';
import { Bill } from './bill';
import { BillService } from './bill.service';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';


@Component({
    moduleId: module.id,
    selector: 'bill-approval',
    templateUrl: 'bill-approval.component.html',
    styleUrls: ['bill-approval.component.css']
})
export class BillApprovalComponent implements OnInit {

    @Input() bill: Bill;

    constructor(
        private billService: BillService,
        public snackBar: MatSnackBar
    ) { }
    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {duration: 2000});
    }

    approve(){
        this.billService.approve(this.bill.Oid).subscribe(
            bill => {
                //do something like openSnackBar()
                this.bill = bill;
                this.openSnackBar(bill.VendorShortcut +" "+ bill.Month +" "+ bill.Year +" bill", 'Approved')
            },
            error => {
                //do something 
                this.openSnackBar(error, 'Something is Wrong!')
            }
        )
    }

    reject(){
        this.billService.reject(this.bill.Oid).subscribe(
            bill => {
                this.bill = bill;
                this.openSnackBar(bill.VendorShortcut + " " + bill.Month + " " + bill.Year + " bill", 'Rejected')
            },
            error => {
                this.openSnackBar(error, 'Something is Wrong!')
            }
        )
    }

    ngOnInit(){

    }
}
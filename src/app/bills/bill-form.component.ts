import { Component, OnInit, Input, Output, HostBinding } from '@angular/core';
import { Bill } from './bill';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { BillService } from './bill.service';
import { VendorService } from '../vendors/vendor.service';
import { Vendor } from '../vendors/vendor';
import { GSECCode } from '../gseccodes/gseccode';
import { GSECCodeService } from '../gseccodes/gseccode.service'
import { Breakdown } from '../breakdowns/breakdown';
import { BreakdownService } from '../breakdowns/breakdown.service';
import 'rxjs/add/operator/switchMap';



@Component({
    moduleId: module.id,
    selector: 'bill-form',
    templateUrl: 'bill-form.component.html',
    styleUrls: [ 'bill-form.component.css']

})
export class BillFormComponent implements OnInit {
    months = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
    years: string[] = []
    vendors: string[] = []
    vendor: Vendor
    message: string;
    gseccodes: GSECCode[];

    enableBreakdown: boolean;

    @Input() bill: Bill;
    @Input() breakdown: Breakdown;
    breakdowns: Breakdown[] = [];
    @Input() gseccode: string;
    @Input() counteraccount: string;
    errorMessage: string;
    submitted: boolean;
    canEdit: boolean;
    remainAmount: number;
    
    

    constructor(
        private billService : BillService,
        private vendorService : VendorService,
        private gseccodeService: GSECCodeService,
        private breakdownService: BreakdownService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router
        ){
            for(var i=0; i<8; i++){
                this.years[i] = (new Date().getFullYear() - 2 + i) + ""
            }
         }

    billReset(){
        
        let abill: any = new Bill();
        abill.Type = "Bill"
        abill.Month = this.months[new Date().getMonth()]
        abill.Year = (new Date().getFullYear()) + ""
        this.bill = abill
        this.vendor = new Vendor()
        this.bill.Breakdowns = []
    }

    setEnableBreakdown(flag: boolean){
        this.enableBreakdown = flag;
    }


    getMonth():number{
        return new Date().getMonth() + 1;
    }

    getVendorList(){
        this.vendorService.getVendorList().subscribe(
            vendors => {
                
                this.vendors = <string[]>vendors},
            error => this.errorMessage = <any>error
        )
    }
    getVendor(shortcut: string){
        this.vendor.Services = []
        if (shortcut === '') {
            return 
        }
   
        this.vendorService.getVendor(shortcut).subscribe(
            vendor => {this.vendor = vendor 
                       this.vendor.Services = vendor.Services || []
                        console.log('got the vendor')},
            error => this.errorMessage = <any>error
        )
    }
    getGSECCodes(){
        this.gseccodeService.getGSECCodes().subscribe(
            gseccodes => {
                this.gseccodes = gseccodes
                },
            error => this.errorMessage = <any>error
        )
    }
    
    payBill(){
        this.bill.IsPaid = true;
        this.billService.update(this.bill)
        .subscribe(bill =>{
            
            this.bill = bill
            this.message = "Paid"
            this.submitted = true;
            this.canEdit = true;
        },
        error => this.errorMessage = <any>error
        )
        
    }
    add():void{
        this.billService.create(JSON.stringify(this.bill))
        .subscribe(
            bill => {
                this.submitted = true;
                this.bill = bill
                this.message = "Created"
                this.submitted = true
                this.remainAmount = <number>bill.Total
                console.log(JSON.stringify((<Bill>bill)))
            },
            error => this.errorMessage = <any>error
        )
        this.breakdown = new Breakdown();

        
    }

    goBack(): void{
        this.location.back();
    }
    edit():void {
        this.submitted = false;
        this.canEdit = false;
        this.message = ""
    }

    update():void {
        this.billService.update(this.bill)
        .subscribe(bill =>{
            this.bill = bill
            this.calcRemainingBill()
            this.message = "Updated"
            this.submitted = true;
            this.canEdit = true;
        },
        error => this.errorMessage = <any>error
        )
        
    }

    // add-breakdown section

    addBreakdowns():void{
        if(this.bill.Oid){
            this.breakdownService;
        }
    }
    addBreakdown():void{
        if (this.bill.Oid){
            this.breakdown.Trailer = this.getTrailer();
            this.bill.Breakdowns.push(this.breakdown)

            /*
            this.breakdownService.addBreakdown(
                this.bill.Oid,
                this.breakdown
            ).subscribe(
                bd => {
                    this.remainAmount -= <number>bd.Amount
                    this.bill.Breakdowns.push(<Breakdown>bd)
                },
                error => this.errorMessage = <any>error
            )
            */
        }
        this.initBreakdown();
    }
    deleteBreakdown(Oid: string):void{

            this.breakdownService.delete(Oid)
            .subscribe(
                bd =>{
                    let index = 0
                    for (var i=0;i<this.bill.Breakdowns.length;i++){
                        if (this.bill.Breakdowns[i].Oid === Oid){
                            index = i
                        }
                    }
                    this.bill.Breakdowns.splice(index,1);
                    this.calcRemainingBill()
                    
                }


            )
       
        
        
    }
    getTrailer():string {
        let trailer = `${this.bill.VendorShortcut}_${this.bill.Month}_${this.bill.Year.substring(2)}`;
        if (this.gseccode != null && this.gseccode != ""){
            trailer = `${trailer}_${this.gseccode}`;
        }
        if (this.breakdown.Detail != null){
            trailer = `${trailer}_${this.breakdown.Detail}`
        } 
        return trailer;
          
    }
 


    initBreakdown(): void{
        this.breakdown = new Breakdown()
        this.gseccode = null
    }

    calcRemainingBill():void{
        this.remainAmount = <number>this.bill.Total
            this.bill.Breakdowns.forEach(
                bd =>{
                    this.remainAmount -= <number>bd.Amount
            })
    }




    ngOnInit(){
        this.getVendorList()
        this.billReset()
        this.getGSECCodes();
        this.submitted = false;
        this.breakdown = new Breakdown();
        this.canEdit = true;
        
        
        let Oid = this.route.snapshot.params['Oid'];
        if(typeof Oid != 'undefined'){
            this.route.params
            .switchMap((params:Params) => 
            this.billService.getBill(Oid)
            )
            .subscribe(bill => {
                this.getVendor(bill.VendorShortcut);
                this.bill = bill
                this.calcRemainingBill()
            });
        }
        
 


        
    }

}
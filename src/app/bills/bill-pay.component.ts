import { Component, OnInit, Input } from '@angular/core';
import { Bill } from './bill';
import { BillService } from './bill.service';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';

@Component({
    moduleId: module.id,
    selector: 'bill-pay',
    templateUrl: 'bill-pay.component.html',
    styleUrls: ['bill-pay.component.css']
})
export class BillPayComponent implements OnInit{
    @Input() bill: Bill;
    ref:string;
    chaseAcct: boolean = false;
    gsecAcct: boolean = false;

    constructor(
        private billService: BillService,
        public snackBar: MatSnackBar
    ) { }
    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {duration: 3500});
    }
    pay(){
        if (this.ref == null){
            this.openSnackBar("Please enter payment confirmation number!", "Not Paid")
        } else if (this.chaseAcct == true || this.gsecAcct == true){
            this.openSnackBar("Please select at least one payment account used!", "Not Paid")
        }
        else {
            this.billService.pay(this.bill.Oid,this.ref, this.chaseAcct, this.gsecAcct).subscribe(
                bill =>{
                    //do something
                    this.bill = bill
                    let msg = `${this.bill.VendorShortcut} ${this.bill.Month} ${this.bill.Year} bill`
                    this.openSnackBar(msg, "Paid")
                },
                error => {
                    //log error
                    this.openSnackBar(error, "Not Paid")
                }
            )
            
        }
    }
    ngOnInit(){

    }
}
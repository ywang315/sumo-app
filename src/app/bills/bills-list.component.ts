import { Component, OnInit } from '@angular/core';
import { Bill } from './bill';
import { BillService } from './bill.service';
import { VendorService } from '../vendors/vendor.service';
import { Vendor } from '../vendors/vendor';
import { VendorComponent } from '../vendors/vendor.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
//import { MdGridList } from '@angular/material';
//import { MdSnackBar } from '@angular/material';
import { MatSnackBar } from '@angular/material';


@Component({
    moduleId: module.id,
    selector: 'bills-list',
    templateUrl: 'bills-list.component.html',
    styleUrls: ["bills-list.component.css"]
})
export class BillsListComponent implements OnInit {
    selectedBill : Bill;
    selectBillOid : string;
    bills : Bill[];
    errorMessage: string;
    
    title: string;


    constructor(
    private billService : BillService,
    private vendorService : VendorService,
    private router : Router,
    private route : ActivatedRoute,
    private location : Location,
    public snackBar: MatSnackBar,
    ){ }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
        duration: 2000,
        });
    }

    getStatus(bill){
        if (bill.IsPaid){
            return 'Paid'
        }else if(bill.IsCompleted){
            return 'Ready to Pay'
        }else {
            return 'Rejected'
        }
    }
    getStyle(bill){
        if(bill.IsPaid){
            return "green"
        }
        else if(bill.IsCompleted){
            return "black"
        }
        else{
            return "red"
        }

  
        
    }
    getBills(args:string) {
      this.billService.getBills(args)
                      .subscribe(
                          bills => {
                              this.bills = bills
                            },
                          error => this.errorMessage = <any>error
                      );
    }



    delete(bill:Bill){
        let index = this.bills.indexOf(bill);
        this.billService.delete(bill)
        .subscribe(
            bill => {
                if (index > -1){
                    this.bills.splice(index,1)
                    this.openSnackBar(bill.VendorShortcut+ " " + bill.Month + " " + bill.Year + " Bill", "Deleted!")
                }
            },
            error => {
                this.openSnackBar(error, "Something went wrong!")
                this.errorMessage = <any>error
            }
        )
        
    }
    cancel(){
        this.openSnackBar("bill not deleted", "Cancelled")
    }
    gotoDetail(bill:Bill){
        this.selectedBill = bill;
        this.router.navigate(['/bills', bill.Oid])
    }

    goBack(){
        this.location.back()
    }
    selectBill(bill:Bill){
        this.selectedBill = bill;
    }





    ngOnInit(){
        let arg = this.route.snapshot.params['arg'];
        if(typeof arg == 'undefined'){
            arg = ""
        }
        this.getBills(arg);
        arg =""
        
        this.title = this.route.snapshot.data['title']

    }
}
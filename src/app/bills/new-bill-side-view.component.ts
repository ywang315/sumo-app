import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Bill } from './bill';
import { Breakdown } from '../breakdowns/breakdown';

@Component({
    moduleId: module.id,
    templateUrl: 'new-bill-side-view.component.html',
    styleUrls: ['new-bill-side-view.component.css'],
    selector: 'bill-side-view'
})
export class NewBillSideViewComponent{
    @Input() bill: Bill;

    @Output() copyBillOver: EventEmitter<Bill> = new EventEmitter<Bill>()
    copy(){
        let breakdowns = []
        this.bill.Breakdowns.forEach(
            bkd =>{
                let b = new Breakdown(bkd.GSECCode, bkd.CounterAccount, bkd.Detail, bkd.Trailer, bkd.Notes,
                                      bkd.Amount, null, bkd.Created, bkd.CreatedBy)
                 breakdowns.push(b)
            }
        )
        let bill = new Bill(this.bill.VendorShortcut, this.bill.Month, this.bill.Year,
                            this.bill.Total, this.bill.Type, this.bill.IsCompleted, this.bill.IsPaid, this.bill.IsApproved,
                            this.bill.Created, breakdowns, null, this.bill.CreatedBy,
                            this.bill.BillNumber, this.bill.BillPayRef)
        this.copyBillOver.emit(bill)
    }
    
}
import { Component, OnInit } from '@angular/core';
import { BillService } from '../bill.service';
import { PaymentSummary } from './payment-summary'
import { Summary } from './summary';

@Component({
    moduleId: module.id,
    selector: 'bills-summary',
    templateUrl: 'bills-summary.component.html',
    styleUrls: ['bills-summary.component.css']
})
export class BillsSummaryComponent implements OnInit{
    paymentSummary: PaymentSummary[];
    errorMessage: string;
    myjson: any = JSON;

    constructor(private billService: BillService) {}

    get_summary(year: string){
        this.billService.getPaymentSummary(year)
                        .subscribe(
                            paymentSummary => {
                                paymentSummary.forEach(
                                    pair => {

                                    }
                                )
                                this.paymentSummary = paymentSummary;
                            },
                            error => this.errorMessage = <any>error
                        )
    }

    ngOnInit(){
        this.get_summary("2017")
    }
}
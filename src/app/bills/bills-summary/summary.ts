export class Summary{
    constructor(
        public JAN: number[],
        public FEB: number[],
        public MAR: number[],
        public APR: number[],
        public MAY: number[],
        public JUN: number[],
        public JUL: number[],
        public AUG: number[],
        public SEP: number[],
        public OCT: number[],
        public NOV: number[],
        public DEC: number[]
    ){}
}
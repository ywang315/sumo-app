import { Summary } from './summary';
export class PaymentSummary{
    constructor(
        public vendor?: string,
        public summary?: Summary,
    ){}
}
import { Vendor } from '../vendors/vendor';
import { Breakdown } from '../breakdowns/breakdown';



export class Bill{
    constructor(
        public VendorShortcut?: string,
        public Month?: string,
        public Year?: string,
        public Total?: number | string,
        public Type?: string,
        public IsCompleted?: boolean,
        public IsPaid?:boolean,
        public IsApproved?: boolean,
        public Created?: Date,
        public Breakdowns?: Breakdown[],
        public Oid?: string,
        public CreatedBy?: Date,
        public BillNumber?: string,
        public BillPayRef?: string,
        public PaymentAccount?: string
    ){    
    }
}


import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillsListComponent } from './bills-list.component';
import { BillFormComponent } from './bill-form.component';
import { NewBillComponent } from './new-bill.component';
import { AuthGuard } from '../auth/auth.guard';
import { BillApprovalViewComponent } from './bill-approval-view.component';
import { BillPayViewComponent } from './bill-pay-view.component';
import { BillsSummaryComponent } from './bills-summary/bills-summary.component'


const billsRoutes: Routes = [
    { path: 'bills', component: BillsListComponent, canActivate: [AuthGuard], data: {title : 'All Bills'}},
    { path: 'bill-form', component: BillFormComponent, canActivate: [AuthGuard]},
    { path: 'bills/:Oid', component: NewBillComponent, canActivate: [AuthGuard] },
    { path: 'bills-paid/:arg', component: BillsListComponent, canActivate: [AuthGuard], data: {title : 'Paid Bills'}},
    { path: 'bills-unpaid/:arg', component: BillsListComponent, canActivate: [AuthGuard], data : {title : 'Rejected Bill'}},
    { path: 'bills-by-vendor/:arg', component: BillsListComponent, canActivate: [AuthGuard], data : {title : 'Bills by Vendor'}},
    { path: 'search-bills/:arg', component: BillsListComponent, canActivate: [AuthGuard], data: {title: 'Approved Bills'}},
    { path: 'new-bill', component: NewBillComponent, canActivate: [AuthGuard]},
    { path: 'approval-view', component: BillApprovalViewComponent, canActivate: [AuthGuard]},
    { path: 'bill-pay', component: BillPayViewComponent, canActivate: [AuthGuard]},
    { path: 'payment-summary', component: BillsSummaryComponent, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [
        RouterModule.forChild(billsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class BillsRoutingModule { }
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var bill_service_1 = require('./bill.service');
var vendor_service_1 = require('../vendors/vendor.service');
var router_1 = require('@angular/router');
require('rxjs/add/operator/switchMap');
var BillsListComponent = (function () {
    function BillsListComponent(billService, vendorService, router, route) {
        this.billService = billService;
        this.vendorService = vendorService;
        this.router = router;
        this.route = route;
    }
    BillsListComponent.prototype.getBills = function () {
        var _this = this;
        this.billService.getBills()
            .subscribe(function (bills) {
            _this.bills = bills;
        }, function (error) { return _this.errorMessage = error; });
    };
    BillsListComponent.prototype.delete = function (bill) {
        var _this = this;
        var index = this.bills.indexOf(bill);
        this.billService.delete(bill)
            .subscribe(function (bill) {
            if (index > -1) {
                _this.bills.splice(index, 1);
                console.log("Deleted: " + bill.Oid);
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    BillsListComponent.prototype.gotoDetail = function (bill) {
        this.selectedBill = bill;
        this.router.navigate(['/bills', bill.Oid]);
    };
    BillsListComponent.prototype.selectBill = function (bill) {
        this.selectedBill = bill;
    };
    BillsListComponent.prototype.ngOnInit = function () {
        this.getBills();
    };
    BillsListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'bills-list',
            templateUrl: 'bills-list.component.html'
        }), 
        __metadata('design:paramtypes', [bill_service_1.BillService, vendor_service_1.VendorService, router_1.Router, router_1.ActivatedRoute])
    ], BillsListComponent);
    return BillsListComponent;
}());
exports.BillsListComponent = BillsListComponent;
//# sourceMappingURL=bills-list.component.js.map
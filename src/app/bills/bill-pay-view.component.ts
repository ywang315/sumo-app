import { Component, OnInit } from '@angular/core';
import { Bill } from './bill';
import { BillService } from './bill.service';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BillPayComponent } from './bill-pay.component';
import { WorkingYearService } from '../local-store/working-year.service';
@Component ({
    moduleId: module.id,
    templateUrl: 'bill-pay-view.component.html',
    styleUrls: ['bill-pay-view.component.css']
})
export class BillPayViewComponent implements OnInit {
    months: string[] = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
    monthNames: string[] = ["January", "February", "March", "April", "May", "June",
                  "July", "August", "September", "October", "November", "December"];
    years: string[] = []
    //selectedYear: string = (new Date().getFullYear()) + "";
    selectedYear: string = this.wys.getYear() + "";
    selectedMonth: number = new Date().getMonth();
    errorMessage: string;

    bills: Bill[];

    constructor(
        private billService : BillService,
        private router : Router,
        private route : ActivatedRoute,
        private wys: WorkingYearService
    ){ 
        for(var i=0; i<8; i++){
                this.years[i] = (new Date().getFullYear() - 2 + i) + ""
            }
    }

    getBills(args:string) {
      this.billService.getBills(args)
                      .subscribe(
                          bills => {
                              this.bills = bills
                            },
                          error => this.errorMessage = <any>error
                      );
    }
    changeTab(index:number){
        this.selectedMonth = index;
        this.getBills('?Year='+this.selectedYear+'&Month='+this.months[index]+'&IsApproved=1&IsPaid=0')
    }

    ngOnInit (){
        this.getBills('?Year='+this.selectedYear+'&Month='+this.selectedMonth+'&IsApproved=1&IsPaid=0')

    }
}
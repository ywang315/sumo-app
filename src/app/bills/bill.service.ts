import { Bill } from './bill';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HOST } from '../host';
import { AuthHttp } from 'angular2-jwt';
import { PaymentSummary } from './bills-summary/payment-summary';

@Injectable()
export class BillService {
  private billsUrl = `${HOST}/bills`
  private headers = new Headers({'Content-Type':'application/json'});
  constructor(private http: AuthHttp){ };


  getPaymentSummary(year: string): Observable<PaymentSummary[]>{
    return this.http.get(`${this.billsUrl}/payment-summary/${year}`)
                    .map(res=>{
                          let body = <PaymentSummary[]>res.json();
                          return body
                    })
                    .catch(this.handleError)
  }


  getBills(args): Observable<Bill[]>{
    return this.http.get(`${this.billsUrl}${args}`)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getLastNBills(vendor: string, n: number): Observable<Bill[]>{
    let url = `${HOST}/vendors/${vendor}/${n}`
    return this.http.get(url)
                    .map(
                      res=>{
                        let body = <Bill[]>res.json();
                        return body || []
                      }
                    )
                    .catch(this.handleError)
  }
  
  approve(Oid:string): Observable<Bill>{
    let url = `${this.billsUrl}/approve`
    return this.http.put(url, {'Oid':Oid}, {headers: this.headers})
            .map(res => {
              let body = <Bill>res.json();
              return body || {}
            })
            .catch(this.handleError)
    }
  reject(Oid:string): Observable<Bill>{
    let url = `${this.billsUrl}/reject`
    return this.http.put(url, {'Oid':Oid}, {headers: this.headers})
          .map(res => {
            let body = <Bill>res.json()
            return body || {}
          })
          .catch(this.handleError)
  }

  pay(Oid:string, ref: string, chaseAcct: boolean, gsecAcct: boolean): Observable<Bill>{
    let url = `${this.billsUrl}/pay`
    return this.http.put(url, {'Oid':Oid, 'BillPayRef':ref, 'PaidThroughChase': chaseAcct, 'PaidThroughGSEC': gsecAcct}, 
          {headers: this.headers})
          .map(res => {
            let body = <Bill>res.json();
            return body || {}
          })
          .catch(this.handleError)
  }


  getBill(Oid:string): Observable<Bill>{

    let url = `${this.billsUrl}/${Oid}`
    return this.http.get(url).map(
      res => {
        let body = <Bill>res.json();
        return body || {};
      }
    )
    .catch(this.handleError)
  }

  create(json_bill:string): Observable<Bill>{
    return this.http
    .post(this.billsUrl,json_bill, {headers: this.headers})
    .map(res =>  {
      let body = <Bill>res.json();
      return body || {};
    })
    .catch(this.handleError)
  }
  update(bill:Bill): Observable<Bill>{
    const url = `${this.billsUrl}/${bill.Oid}`;
    return this.http
    .put(url, JSON.stringify(bill), {headers: this.headers})
    .map(res => {
      let body = <Bill>res.json();
      return body || {};
    })
    .catch(this.handleError)
  }


  delete(bill:Bill): Observable<Bill>{
    const url = `${this.billsUrl}/${bill.Oid}`;
    return this.http
    .delete(url,{headers: this.headers})
    .map(
      res => {
        let body = <Bill>res.json();
        return body || {};
      }
    )
    .catch(this.handleError)
  }



  private extractData(res: Response){
    let body = <Bill[]>res.json();
    return body || [];
  }


  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
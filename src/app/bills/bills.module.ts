import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BillsListComponent } from './bills-list.component';
import { BillFormComponent } from './bill-form.component';

import { BillService } from './bill.service';
import { BillsSummaryComponent } from './bills-summary/bills-summary.component';
import { BillsRoutingModule } from './bills-routing.module';

import { BreakdownComponent } from '../breakdowns/breakdown.component';
//import { MaterialModule } from '@angular/material';
import { NewBillComponent } from './new-bill.component';
import { BillApprovalViewComponent } from './bill-approval-view.component';
import { BillApprovalComponent } from './bill-approval.component';
import { BillPayViewComponent } from './bill-pay-view.component';
import { BillPayComponent } from './bill-pay.component';
import { NewBillSideViewComponent } from './new-bill-side-view.component';
import { NgPipesModule } from 'ngx-pipes';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { LocalStoreModule } from '../local-store/local-store.module';
import { Ng2CompleterModule } from "ng2-completer";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import { BreakdownTableComponent } from '../breakdowns/breakdown-table.component';
import {MatSortModule} from '@angular/material/sort';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
    imports: [
        MatIconModule,
        MatDialogModule,
        MatSortModule,
        MatTableModule,
        MatButtonModule,
        MatGridListModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatTooltipModule,
        MatSelectModule,
        MatCardModule,
        MatToolbarModule,
        MatTabsModule,
        MatInputModule,
        MatCheckboxModule,
        //MaterialModule,
        MatSnackBarModule,
        Ng2CompleterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BillsRoutingModule,
        NgPipesModule,
        LocalStoreModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger' // set defaults here
          })
    ],
    declarations: [
        BillPayComponent,
        BillPayViewComponent,
        BillApprovalComponent,
        BillApprovalViewComponent,
        BillFormComponent,
        BillsListComponent,
        BillsSummaryComponent,
        NewBillComponent,
        BreakdownComponent,
        NewBillSideViewComponent,
        BreakdownTableComponent,
    ],
    providers: [ BillService, CurrencyPipe ],
    exports: [
        BreakdownComponent,
    ]
})
export class BillsModule {}
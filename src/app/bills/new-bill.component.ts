import { Component, OnInit, Input, Output, DoCheck, Pipe, PipeTransform} from '@angular/core';
import { Bill } from './bill';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Location, CurrencyPipe } from '@angular/common';
import { BillService } from './bill.service';
import { VendorService } from '../vendors/vendor.service';
import { Vendor } from '../vendors/vendor';
import { GSECCode } from '../gseccodes/gseccode';
import { GSECCodeService } from '../gseccodes/gseccode.service'
import { Breakdown } from '../breakdowns/breakdown';
import { BreakdownService } from '../breakdowns/breakdown.service';
import { BreakdownComponent } from '../breakdowns/breakdown.component';
import { NewBillSideViewComponent } from './new-bill-side-view.component';
import 'rxjs/add/operator/switchMap';
import { ReversePipe } from 'ngx-pipes/src/app/pipes/array/reverse';
import { CompleterService, CompleterData } from 'ng2-completer';
import {MatDialog} from '@angular/material';
import { ConfirmDialogComponent } from '../confirm-dialog.component';


import {FormControl} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

@Component({
    moduleId: module.id,
    selector: 'new-bill',
    templateUrl: 'new-bill.component.html',
    styleUrls: [ 'new-bill.component.css']

})
export class NewBillComponent implements OnInit, DoCheck {
    months = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
    years: string[] = []
    vendors: string[] = []
    selectedVendor: Vendor;
    errorMessage: string;
    gseccodes: GSECCode[];
    breakdown: Breakdown = new Breakdown();
    gs_code_str: string[];
    displayTotalAmount: any;
    runningSum: number;

    filteredVendors: Observable<string[]>;
    vendorCtrl: FormControl = new FormControl();


    @Input()
    bill: Bill;
    billsLastMonth: Bill[];

    constructor(
        private billService : BillService,
        private vendorService : VendorService,
        private gseccodeService: GSECCodeService,
        private breakdownService: BreakdownService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        public snackBar: MatSnackBar,
        public dialog: MatDialog,
        public cp: CurrencyPipe
        ){
            for(var i=0; i<8; i++){
                this.years[i] = (new Date().getFullYear() - 2 + i) + ""
            }
         }


    setTotalAmountValue(event){
        //sets the bill total in number format
        this.bill.Total = +event.replace(new RegExp(/[,|$]/g), "")
    }

    updateDisplayAmount(){
        //formats the input as currency when focusout or in edit mode
        if (this.bill.Total){
            this.displayTotalAmount = (""+this.bill.Total).replace(new RegExp(/[$]/g), "")
        }
    }

    filtereVendors(vendor_arg: string) {
        return this.vendors.filter(vendor => vendor_arg? 
            vendor.toLowerCase().indexOf(vendor_arg.toLowerCase()) === 0: this.vendors.slice());
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
        duration: 2000,
        })
        ;
    }


    confirmDialog(title: string, content: string){
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            height: '',
            data: {title: title, content: content}
        });

        return dialogRef.afterClosed()
        
    }
    createOrUpdateBill(){
        let msg:string;
        if (this.bill.Oid){
            msg = "You are about to update the bill"
        }else {
            msg = "You are about to create a new bill"
        }
        this.confirmDialog("Do you want to preceed?", msg).subscribe(
            res => {
                if (res === true && this.bill.Oid){
                    this.updateBill()
                }
                else if(res === true){
                    this.createBill()
                }
            }
        )
    }

    createBill(){
        this.billService.create(JSON.stringify(this.bill)).subscribe(
            bill =>{
                //do something
                this.bill = bill;
                this.openSnackBar("New Bill Created", "Success!")
            },
            error => {
                //log error
                this.openSnackBar(error,"Failed!")
            }
        )
        this.initBillCreation()
    }

    updateBill(){
        this.bill.IsApproved = false
        this.billService.update(this.bill).subscribe(
            bill =>{
                //do something
                this.bill = bill;
                this.openSnackBar(this.bill.VendorShortcut + " bill is updated", "Success!")

            },
            error => {
                //log error
                this.openSnackBar(error,"Failed!")
            }
        )
    }

    get total():number {
        if(this.bill.Breakdowns.length > 0){
            return this.bill.Breakdowns.map(bkd => + bkd.Amount).reduce((a, b) => a + b)
        }
        else{
            return 0
        }
        
    }
    get disabled() {
        if (this.bill.Total){
            let diff = Math.round((this.total - +this.bill.Total) * 100) / 100

            return diff < 0
        }
        return true;
    }

    initBillCreation(){
        this.bill = new Bill();
        this.bill.Type = "Bill"    
        this.bill.Breakdowns = [] 
        this.bill.Year = (new Date().getFullYear()) + ""
        this.bill.Month = this.months[new Date().getMonth()]
        this.billsLastMonth = null;

    }

    getVendorList(){
        this.vendorService.getVendorList().subscribe(
            vendors => {
                this.vendors = <string[]>vendors;
                this.filteredVendors = this.vendorCtrl.valueChanges
                .pipe(
                    startWith(''),
                    map(vendor =>  this.filtereVendors(vendor))
                );
            },
            error => this.errorMessage = <any>error
        )
    }
  

    getVendor(shortcut: string){
        //this.getLatestNBills()
        if (shortcut === '') {
            this.selectedVendor = new Vendor();
            this.selectedVendor.Services = [];
            return 
        }
        this.getLatestNBills(shortcut)
        this.bill.VendorShortcut = shortcut
   
        this.vendorService.getVendor(shortcut).subscribe(
            vendor => {this.selectedVendor = vendor 
                       this.selectedVendor.Services = vendor.Services || []
            },
            error => this.errorMessage = <any>error
        )
    }
    getGSECCodes(){
        this.gseccodeService.getGSECCodes().subscribe(
            gseccodes => {
                this.gseccodes = <GSECCode[]>gseccodes
                },
            error => this.errorMessage = <any>error
        )
    }
    addBreakdown(bkd:Breakdown){
        this.bill.Breakdowns.push(bkd)
    }

 

    removeBreakdown(index:number){
        let temp = this.bill.Breakdowns[index]
        let msg = `Are you sure you want to delete  
         [ No.${index + 1} ]  [ ${temp.GSECCode} ]  [${this.cp.transform(temp.Amount,'USD')}]`
        this.confirmDialog("Removing Line Item", msg).subscribe(
            res => {
                if (res === true){
                    this.bill.Breakdowns.splice(index,1)
                    this.openSnackBar("Removed", `${temp.GSECCode} - ${this.cp.transform(temp.Amount,'USD')}`)
                }
                else{
                    this.openSnackBar("Cancelled", "")
                }
            }
        )
        
        
        /*
        if (temp.Oid != null){
            this.breakdownService.delete(temp.Oid).subscribe(
                bd =>{
                    //confirm deletion
                }
            )
        } else {
            //remove from local
        }
        */
    }

    goBack(): void{
        this.location.back();
    }

    reSet(){
        this.initBillCreation()
    }



    getBillsLastMonth(){
        let month_index = this.months.indexOf(this.bill.Month)
        let month = this.months[(month_index + 11) % 12]

        let year = this.bill.Year

        if (month_index == 0){
            let year = parseInt(this.bill.Year) -1
        } 

        let arg = `?VendorShortcut=${this.bill.VendorShortcut}&Year=${year}&Month=${month}`
        this.billService.getBills(arg).subscribe(
            bills =>{
                this.billsLastMonth = bills
            },
            error => {
                this.errorMessage = <any>error;
                this.billsLastMonth = undefined
            }
        )
    }
    getLatestNBills(vendor: string){
        let n = 3;
        this.billService.getLastNBills(vendor, n).subscribe(
            bills => this.billsLastMonth = bills,
            error => {
                this.errorMessage = <any>error;
                this.billsLastMonth = undefined
            }
        )
    }

    showLastBill(){
        if(this.billsLastMonth == undefined){
            return false
        } 
        else if(this.billsLastMonth.length == 0){
            return false
        }else {
            return true
        }
    }

    fromLastMonth(bill: Bill){
        
        let month_index = this.months.indexOf(bill.Month)
        let month = this.months[(month_index + 1) % 12]
        if(month_index == 11){
            bill.Year = (parseInt(bill.Year) + 1) + ""
        }
        //bill.Month = month
        bill.Month = this.bill.Month
        this.bill = bill
        this.updateDisplayAmount()

    }


    ngOnInit(){
        this.initBillCreation()
        this.getVendorList()
        this.getGSECCodes()
  
        let Oid = this.route.snapshot.params['Oid'];
        if(typeof Oid != 'undefined'){
            this.route.params
            .switchMap((params:Params) => 
            this.billService.getBill(Oid)
            )
            .subscribe(bill => {
                this.getVendor(bill.VendorShortcut);
                this.bill = bill
                this.updateDisplayAmount()
            });
        }

        this.getBillsLastMonth()

        
    }


    
    ngDoCheck(){

    }
}
import { MonthlyExpenses } from './monthlyexpenses';
import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { HOST } from '../host';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class MonthlyExpensesService {
    private expUrl = `${HOST}/monthlyexpenses`
    private headers = new Headers({'Content-Type':'application/json'});

    constructor(private http: AuthHttp) { };


    updateEstimate(line:string): Observable<MonthlyExpenses> {
        let url = `${this.expUrl}/${line}`
        return this.http.put(url, this.headers)
        .map(
            res => {
                let body = <MonthlyExpenses>res.json();
                return body || {}
            }
        )
        .catch(this.handleError)
    }
    

    private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
export class MonthlyExpenses{
    constructor(
        public Oid?: string,
        public Month?: string,
        public Year?: number,
        public AverageExpense?: number | string,
        public RealExpense?: number | string,
        public Balance?: number | string,
        public Created?: Date
    ){}


}
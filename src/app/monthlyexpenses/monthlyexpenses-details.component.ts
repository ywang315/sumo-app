import { Component, Input } from '@angular/core';
import { MonthlyExpenses } from './monthlyexpenses';

@Component({
    moduleId: module.id,
    selector: 'monthlyexpense',
    templateUrl: 'monthlyexpenses-details.component.html'
})
export class MonthlyExpensesDetailsComponent {
    @Input() expense: MonthlyExpenses;
}